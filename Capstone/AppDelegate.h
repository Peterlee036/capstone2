//
//  AppDelegate.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/6.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>
#import "Tools.h"
#import "Repository/UsersRepository.h"
@import Firebase;
@interface AppDelegate : UIResponder <UIApplicationDelegate,ConnectObjDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIStoryboard *Storyboard;
@property (strong, nonatomic) UsersRepository *me;
@property (strong, nonatomic) Tools *tools;
@property (strong, nonatomic) NSNumber *notifyBadge;
@property RLMRealm *DefaultRealm;

-(float)getNavigationHeight;
@end

