//
//  Tools.m
//  HS_Member
//
//  Created by Peter Lee on 2018/10/19.
//  Copyright © 2018 Peter Lee. All rights reserved.
//

#import "Tools.h"
#import <CommonCrypto/CommonCrypto.h>
@implementation Tools
+(float)Get_StatusBar_Height{
    float statusHeight = 20;
    if (@available(iOS 11.0, *)) {
         statusHeight = [[UIApplication sharedApplication] keyWindow].safeAreaInsets.top;
        if (statusHeight == 0){
            statusHeight = 20;
        }
    }
    return statusHeight;
}
+(BOOL)is_iphoneX_screen{
    if (@available(iOS 11.0, *)) {
        NSLog(@"inset %@",NSStringFromUIEdgeInsets([UIApplication sharedApplication].keyWindow.safeAreaInsets));
        return NO;
    }else{
        return NO;
    }
}
+(NSString *)NSStringtoMD5:(NSString *)input{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (int)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}
+(BOOL)isEmailFormat:(NSString*)email{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}
+(BOOL)isIdNumberFormat:(NSString *)id_number{
    // 確認身分證字號格式
    NSString *phoneRegex = @"[A-Z][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:id_number];
    return  matches;
}
+(BOOL)ifPhoneNumberCharacterSet:(NSString*)Text{
    NSCharacterSet *ProhibitedInput = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    if (([Text rangeOfCharacterFromSet:ProhibitedInput].location == NSNotFound) && [Text length] == 10 ){
        return YES;
    }else{
        return NO;
    }
}
+(deviceType)getCurrentDeviceType{
    // return 0 ipad ,1 iphone 6 up ,2 iphone rest
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        //ipad
        if (IS_IPAD_PRO){
            return deviceTypeiPadPro;
        }else{
            return deviceTypeiPad;
        }
    }else{
        if (IS_IPHONE_4_OR_LESS){
            return  deviceTypeiPhone4s;
        }else if (IS_IPHONE_5){
            return  deviceTypeiPhone5;
        }else if (IS_IPHONE_6){
            return  deviceTypeiPhone6;
        }else if (IS_IPHONE_6P){
            return  deviceTypeiPhonePlus;
        }else if (IS_IPHONE_X){
            return deviceTypeiPhoneX;
        }else{
            return deviceTypeDefault;
        }
    }
}
+(UIImage *)GeneratorQRCodeFromString:(NSString *)code size:(CGFloat)size{
      NSLog(@" qrcode %@",code);
    //创建CIFilter 指定filter的名称为CIQRCodeGenerator
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    //指定二维码的inputMessage,即你要生成二维码的字符串
       [filter setValue:[code dataUsingEncoding:NSUTF8StringEncoding] forKey:@"inputMessage"];
    //输出CIImage
    CIImage *ciImage = [filter outputImage];
    //对CIImage进行处理
    if (ciImage){
        return [self createfNonInterpolatedImageFromCIImage:ciImage withSize:size];
    }else{
        NSLog(@"ciImage %@",ciImage);
        return nil;
    }
    
}

+(UIImage *) createfNonInterpolatedImageFromCIImage:(CIImage *)iamge withSize:(CGFloat)size{
    CGRect extent = iamge.extent;
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    size_t with = scale * CGRectGetWidth(extent);
    size_t height = scale * CGRectGetHeight(extent);
    
    UIGraphicsBeginImageContext(CGSizeMake(with, height));
    CGContextRef bitmapContextRef = UIGraphicsGetCurrentContext();
    
    CIContext *context = [CIContext contextWithOptions:nil];
    //通过CIContext 将CIImage生成CGImageRef
    CGImageRef bitmapImage = [context createCGImage:iamge fromRect:extent];
    //在对二维码放大或缩小处理时,禁止插值
    CGContextSetInterpolationQuality(bitmapContextRef, kCGInterpolationNone);
    //对二维码进行缩放
    CGContextScaleCTM(bitmapContextRef, scale, scale);
    //将二维码绘制到图片上下文
    CGContextDrawImage(bitmapContextRef, extent, bitmapImage);
    //获得上下文中二维码
    UIImage *retVal =  UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(bitmapImage);
    CGContextRelease(bitmapContextRef);
    return retVal;
}

@end
