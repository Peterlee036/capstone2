//
//  MainTabbarController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/7.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTabbarController : UITabBarController
@property int defaultIndexPage;
@end

NS_ASSUME_NONNULL_END
