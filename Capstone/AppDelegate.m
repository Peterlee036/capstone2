//
//  AppDelegate.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/6.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "AppDelegate.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize Storyboard;
@synthesize DefaultRealm,me;
@synthesize tools;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self initStoreboard];
    [self registerForRemoteNotifications];
    DefaultRealm = [RLMRealm defaultRealm];
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    return YES;
}
#pragma mark - init Method
-(void)initStoreboard{
    switch ([Tools getCurrentDeviceType]) {
        case deviceTypeiPhone4s:
        case deviceTypeiPhone5:
        case deviceTypeiPhone6:
            NSLog(@"storyboard iphone6");
            self.Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            break;
            
        default:
            NSLog(@"storyboard main");
            self.Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            break;
    }
}

#pragma mark - FCM Delegate
-(void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken{
    NSLog(@"fcmToken %@",fcmToken);
    [self.me updateUserFirebaseToken:fcmToken];
}
#pragma mark Push Notification Delegate

-(void)registerForRemoteNotifications{
    if ([UNUserNotificationCenter class] != nil) {
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
         }];
    }
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)nUserInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"背景點擊時會執行  %@,%li",nUserInfo,application.applicationState);
    int badge = (int)application.applicationIconBadgeNumber +1;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"isNotificationAction"];
    [[NSUserDefaults standardUserDefaults] setObject:nUserInfo forKey:@"NotificationInfo"];
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"BackgroundNotification" object:nil userInfo:nUserInfo];
     NSLog(@"nUserInfo  %@,%@",[nUserInfo objectForKey:@"data"],[nUserInfo objectForKey:@"callID"]);
    completionHandler(UIBackgroundFetchResultNewData);
    
    //一定要寫在 completHandler 後面, 應該是執行緒的問題
    [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification
    withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * nUserInfo = notification.request.content.userInfo;
    NSLog(@"前景時會執行  %@",nUserInfo);
   [[NSNotificationCenter defaultCenter] postNotificationName:@"ForegrundNotification" object:nil userInfo:nUserInfo];
    completionHandler(UNNotificationPresentationOptionSound);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(float)getNavigationHeight{
    return 44 + [Tools Get_StatusBar_Height];
}
#pragma mark - ConnectObjDelegate
//-(void)connectionStatusChanged:(AFNetworkReachabilityStatus)status{
//    self.NetworkStatus = status;
//    switch (status) {
//        case AFNetworkReachabilityStatusUnknown:
//            NSLog(@"網路狀況未知");
//            break;
//        case AFNetworkReachabilityStatusNotReachable:
//            NSLog(@"无网络");
//            break;
//        case AFNetworkReachabilityStatusReachableViaWWAN:
//            NSLog(@"行動網路");
//            break;
//        case AFNetworkReachabilityStatusReachableViaWiFi:
//            NSLog(@"Wifi");
//            break;
//        default:
//            break;
//    }
//}
//-(void)connectionLost:(ConnectObj *)connectObj{
//    NSLog(@"time out");
////    [self ShowAlertWithTitle:@"網路連線問題" Message:@"目前無法連線至伺服器，請檢查網路連線狀態，或稍後再試"];
//}
//-(void)connectionTimeout:(ConnectObj *)connectObj{
//    NSLog(@"network lost");
////    [self ShowAlertWithTitle:@"網路連線問題" Message:@"連線逾時，請在網路連線較佳的情況下再使用服務"];
//}
//-(void)connectionUnAuth:(ConnectObj *)connectObj{
////    if ([connectObj.Type isEqualToString:@"auth/login/businesscounter"]){
////        [self ShowAlertWithTitle:@"登入錯誤" Message:@"請輸入正確的帳號密碼"];
////    }else{
////        //token 過期
////        [self Logout];
////    }
////    
//}

@end
