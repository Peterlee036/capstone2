//
//  MainTabbarController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/7.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "MainTabbarController.h"

@interface MainTabbarController ()

@end

@implementation MainTabbarController
@synthesize defaultIndexPage;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBar appearance] setTintColor: [UIColor colorWithRed:89/255.0f green:125/255.0f blue:138/255.0f alpha:1]];
   // [[UITabBar appearance] setShadowImage:nil];
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
}
-(void)viewWillAppear:(BOOL)animated{
    UITabBarItem *homeItem = [self.tabBar.items objectAtIndex:2];
    UIImage *HomeImg = [UIImage imageNamed:@"tabbar_home.png"] ;
    HomeImg = [HomeImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [homeItem setImage:HomeImg];
    [homeItem setSelectedImage:HomeImg];
}
- (void)viewWillLayoutSubviews{
    if (@available(iOS 11.0, *)) {
        CGFloat tabbar_height = 57 + [[UIApplication sharedApplication] keyWindow].safeAreaInsets.bottom;
        CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
        tabFrame.size.height = tabbar_height;
        tabFrame.origin.y = self.view.frame.size.height - tabbar_height;
        self.tabBar.frame = tabFrame;
    }
    [self setSelectedIndex:defaultIndexPage];
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
@end
