//
//  Customer.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "Customer.h"

@implementation Customer
@synthesize ID,name,tel,cellphone,remarks,address;
- (id) init{
    if (self = [super init]){
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}
-(void)getCustomerReady:(NSDictionary*)data{
    self.ID = [data objectForKey:@"id"];
    self.name = [data objectForKey:@"name"];
    self.cellphone = [data objectForKey:@"cellphone"];
    self.tel = [data objectForKey:@"tel"];
    self.remarks = [data objectForKey:@"remarks"];
    self.address = [data objectForKey:@"address"];
}
@end
