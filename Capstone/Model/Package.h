//
//  Package.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Package : RLMObject{
    NSDateFormatter *dateformatter;
}
@property NSString *ID;
@property NSString *type;
@property NSString *status;
@property NSString *takeResidentID;
@property NSDate *put_at;
@property NSDate *take_at;
-(void)getPackageReady:(NSDictionary*)data;
-(NSString *)getTimestampString:(NSDate*)date;
-(NSString*)getLastingTime;
@end


