//
//  Call.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "Call.h"
@implementation Call
@synthesize ID,customerID,placeID,householdID;
@synthesize callRoom,callPeople,callResidentID;
@synthesize start_at,end_at,during;
@synthesize status,action;

- (id) init{
    if (self = [super init]){
        NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
        NSTimeZone *dateTimezone =  [NSTimeZone timeZoneWithName:@"Asia/Taipei"];
        dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setLocale:datelocale];
        [dateformatter setTimeZone:dateTimezone];
        [dateformatter setDateFormat:@"YYYYMMdd HHmmss"];
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}
-(void)getCallReady:(NSDictionary*)data{
    self.ID = [data objectForKey:@"id"];
    self.customerID = [data objectForKey:@"customerID"];
    self.placeID = [data objectForKey:@"placeID"];
    self.householdID = [data objectForKey:@"householdID"];
    
    self.callPeople = [data objectForKey:@"callPeople"];
    self.callRoom = [data objectForKey:@"callRoom"];
    self.callResidentID = [data objectForKey:@"callResidentID"];
    
    self.start_at = [dateformatter dateFromString:[NSString stringWithFormat:@"%@ %@",[data objectForKey:@"startDate"],[data objectForKey:@"startTime"]]];
    self.end_at = [dateformatter dateFromString:[NSString stringWithFormat:@"%@ %@",[data objectForKey:@"endDate"],[data objectForKey:@"endTime"]]];
    [self getDuration];
    self.action = [data objectForKey:@"action"];
    self.status = [data objectForKey:@"status"];
}
-(void)getDuration{
    int Duration = round([self.end_at timeIntervalSinceDate:self.start_at]);
    int min = Duration / 60;
    int sec = Duration % 60;
    during = [NSString stringWithFormat:@"%i分%i秒",min,sec];
}
-(NSString *)getTimestampString{
    NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    NSTimeZone *dateTimezone =  [NSTimeZone timeZoneWithName:@"Asia/Taipei"];
    NSDateFormatter *output_dateformatter = [[NSDateFormatter alloc] init];
    [output_dateformatter setLocale:datelocale];
    [output_dateformatter setTimeZone:dateTimezone];
    [output_dateformatter setDateFormat:@"YYYY.MM.dd HH:mm"];
    return [NSString stringWithFormat:@"%@ | %@",[output_dateformatter stringFromDate:self.start_at],self.during];
}
@end
