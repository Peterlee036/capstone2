//
//  Notify.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

typedef NS_ENUM(NSInteger, NotifyActionType) {
    NotifyActionTypeUnknow = 0,
    NotifyActionTyePackage,
    NotifyActionTypeTake
};

typedef NS_ENUM(NSInteger, NotifyPeopleType) {
    NotifyPeopleTypeUnknow = 0,
    NotifyPeoplePostMan,
    NotifyPeopleLogistics
};

@interface Notify : RLMObject{
    NSDateFormatter *dateformatter;
}
@property NSNumber<RLMInt> *ID;
@property NSNumber<RLMInt> *ActionType;
@property NSNumber<RLMInt> *PeopleType;
@property NSString *name;
@property NSNumber<RLMBool> *isReady;
@property NSDate *put_at;
@property NSDate *create_at;
-(void)getNotifyReady:(NSDictionary*)data;
-(NSString *)getTimestampString:(NSDate*)date;
-(NSString*)getLastingTime;
@end
