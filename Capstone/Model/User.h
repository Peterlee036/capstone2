//
//  User.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/7.
//  Copyright © 2019 Peter Lee. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

typedef NS_ENUM(int, IdentityType) {
    IdentityTypeUnknow = 0,
    IdentityTypeLeader,
    IdentityTypeResident
};
@interface User : RLMObject

@property NSString *ID;
@property NSString *name;
@property NSString *cellphone;
@property NSString *email;
@property NSString *takePassword; //領取包裹密碼
@property NSString *firebaseToken; //推播token
@property NSString *token; // md5(yyyymmdd), 只有註冊時候用
@property NSString *accessToken; // jwt tokwn
@property NSString *remarks; // 備註事項
@property NSNumber<RLMInt> *IdentityStatus; // leader住戶代表：resident一般住戶
@property NSString *certificationQRCODE; // 驗證QRCODE
@property NSString *shareQRCODE; // 分享QRCODE
@property NSString *takeQRCODE; // 取物QRCODE
@property NSString *placeName;
@property NSString *placeAddress;

-(void)getUserReady:(NSDictionary*)data;
-(void)updateUser:(NSDictionary*)data;
@end

