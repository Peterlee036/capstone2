//
//  Package.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "Package.h"

@implementation Package
@synthesize ID,type,status,takeResidentID;
@synthesize put_at,take_at;
- (id) init{
    if (self = [super init]){
        NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
        NSTimeZone *dateTimezone =  [NSTimeZone timeZoneWithName:@"Asia/Taipei"];
        dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setLocale:datelocale];
        [dateformatter setTimeZone:dateTimezone];
        [dateformatter setDateFormat:@"YYYYMMdd HHmmss"];
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}
-(void)getPackageReady:(NSDictionary*)data{
    self.ID = [data objectForKey:@"id"];
    self.takeResidentID = [data objectForKey:@"takeResidentID"];
    self.type = [data objectForKey:@"type"];
    self.status = [data objectForKey:@"status"];
    if ([[data objectForKey:@"putDate"] length] > 0){
         self.put_at = [dateformatter dateFromString:[NSString stringWithFormat:@"%@ %@",[data objectForKey:@"putDate"],[data objectForKey:@"putTime"]]];
    }
    if ([[data objectForKey:@"takeDate"] length] > 0){
        self.take_at = [dateformatter dateFromString:[NSString stringWithFormat:@"%@ %@",[data objectForKey:@"takeDate"],[data objectForKey:@"takeTime"]]];
    }
}
-(NSString *)getTimestampString:(NSDate*)date{
    if (date){
        NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
        NSTimeZone *dateTimezone =  [NSTimeZone timeZoneWithName:@"Asia/Taipei"];
        NSDateFormatter *output_dateformatter = [[NSDateFormatter alloc] init];
        [output_dateformatter setLocale:datelocale];
        [output_dateformatter setTimeZone:dateTimezone];
        [output_dateformatter setDateFormat:@"YYYY.MM.dd HH:mm"];
        return [output_dateformatter stringFromDate:date];
    }else{
        return @"";
    }
}
-(NSString*)getLastingTime{
    int LastingTime;
    if (!self.take_at){
        LastingTime = round([[NSDate date] timeIntervalSinceDate:self.put_at]);
    }else{
       LastingTime = round([self.take_at timeIntervalSinceDate:self.put_at]);
    }
    int day = LastingTime / 86400;
    int hour = (LastingTime % 86400) / 3600;
    return [NSString stringWithFormat:@"%i天%i小時",day,hour];
}
@end

