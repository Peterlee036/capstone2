//
//  ConnectObj.h
//  HS_Member
//
//  Created by Peter Lee on 2018/10/24.
//  Copyright © 2018 Peter Lee. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "AFNetworking/AFNetworking.h"


NS_ASSUME_NONNULL_BEGIN
typedef void (^ConnectStatusNormalHandler)(long StatusCode,NSString *Type,NSDictionary *Result);
typedef void (^ConnectStatusErrorHandler)(long StatusCode,NSString *Message,NSDictionary *Result);

@class ConnectObj;
@protocol ConnectObjDelegate <NSObject>
@optional
-(void)connectionStatusChanged:(AFNetworkReachabilityStatus)status;
-(void)connectionLost:(ConnectObj*)self;
-(void)connectionTimeout:(ConnectObj*)self;
-(void)connectionUnAuth:(ConnectObj*)self;
@end

@interface ConnectObj : NSObject

@property(nonatomic,strong) AFHTTPSessionManager *AFSManager;
@property(nonatomic,strong) id delegate;
@property(nonatomic,strong) NSString *Type;
@property(nonatomic,strong) NSString *Method;
@property(nonatomic,strong) NSMutableURLRequest *Request;
@property(nonatomic,strong) NSDictionary *Result;
@property (nonatomic, copy) ConnectStatusNormalHandler ConnectSuccessHandler;
@property (nonatomic, copy) ConnectStatusErrorHandler ConnectFailHandler;

+(NSString*)getAPIURL;
+(NSString*)getHostURL;

//-(void)connectAPIWithImage:(UIImage*)image  Parameter:(nullable NSDictionary*)Parameter Type:(NSString*)Type
//                  Success:(nullable void (^)(long StatusCode,NSString *Type,NSDictionary *Result))SuccessHandler
//                     Fail:(nullable void (^)(long StatusCode,NSString *Message,NSDictionary *Result))FailHandler;

-(void)connectAPIWithType:(NSString*)Type Parameter:(nullable NSDictionary*)Parameter Method:(NSString*)method useToken:(BOOL)shouldToken
                  Success:(nullable void (^)(long StatusCode,NSString *Type,NSDictionary *Result))SuccessHandler
                     Fail:(nullable void (^)(long StatusCode,NSString *Message,NSDictionary *Result))FailHandler;

+(BOOL)isTestServer;
-(void)reTryConnect;
@end

NS_ASSUME_NONNULL_END
