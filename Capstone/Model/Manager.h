//
//  Manager.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Customer.h"

@interface Manager : RLMObject
@property NSString *ID;
@property NSString *name;
@property NSString *placeID;
@property NSString *cellphone;
@property NSString *address;
@property NSString *remarks;
@property NSString *customerID;
@property Customer *customer;
-(void)getManagerReady:(NSDictionary*)data;
@end

