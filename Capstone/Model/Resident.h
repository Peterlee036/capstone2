//
//  Resident.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Resident : RLMObject

@property NSString *ID;
@property NSString *name;
@property NSString *cellphone;
@property NSString *email;
@property NSString *remarks; // 備註事項
-(void)getResidentReady:(NSDictionary*)data;
@end

