//
//  Resident.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "Resident.h"

@implementation Resident
@synthesize ID,email,cellphone,remarks,name;
- (id) init{
    if (self = [super init]){
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}
-(void)getResidentReady:(NSDictionary*)data{
    self.ID = [data objectForKey:@"id"];
    self.name = [data objectForKey:@"name"];
    self.cellphone = [data objectForKey:@"cellphone"];
    self.email = [data objectForKey:@"email"];
    self.remarks = [data objectForKey:@"remarks"];
}
@end
