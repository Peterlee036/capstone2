//
//  Manager.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "Manager.h"

@implementation Manager
@synthesize ID,name,cellphone,placeID,remarks,address;
@synthesize customer,customerID;
- (id) init{
    if (self = [super init]){
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}
-(void)getManagerReady:(NSDictionary*)data{
    self.ID = [data objectForKey:@"id"];
    self.name = [data objectForKey:@"name"];
    self.cellphone = [data objectForKey:@"cellphone"];
    self.remarks = [data objectForKey:@"remarks"];
    self.address = [data objectForKey:@"address"];
    self.customerID = [data objectForKey:@"customerID"];
    self.placeID = [data objectForKey:@"placeID"];
}
@end
