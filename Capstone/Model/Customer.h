//
//  Customer.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
NS_ASSUME_NONNULL_BEGIN

@interface Customer : RLMObject

@property NSString *ID;
@property NSString *name;
@property NSString *cellphone;
@property NSString *tel;
@property NSString *address;
@property NSString *remarks;
-(void)getCustomerReady:(NSDictionary*)data;
@end

NS_ASSUME_NONNULL_END
