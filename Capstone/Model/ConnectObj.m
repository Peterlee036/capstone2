//
//  ConnectObj.m
//  HS_Member
//
//  Created by Peter Lee on 2018/10/24.
//  Copyright © 2018 Peter Lee. All rights reserved.
//

#import "ConnectObj.h"
#import "../AppDelegate.h"

#define TEST_HOST_URL @"http://cloudboxdev.chiliman.com.tw" //測試機
#define TEST_API_URL @"http://cloudboxdev.chiliman.com.tw/api/" //測試機

#define HOST_URL @"http://cloudboxdev.chiliman.com.tw" //正式機
#define API_URL @"ttp://cloudboxdev.chiliman.com.tw/api/" //正式機
@implementation ConnectObj
@synthesize AFSManager,Request,Result;
@synthesize Type,Method;
@synthesize ConnectSuccessHandler,ConnectFailHandler;

- (id) init{
    if (self = [super init]){
        self.AFSManager =  [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        self.AFSManager.responseSerializer = [AFJSONResponseSerializer serializer];
        self.AFSManager.responseSerializer.acceptableContentTypes =  [NSSet setWithObjects:@"text/html",@"application/json",@"image/jpeg",nil];
        [self.AFSManager.requestSerializer setTimeoutInterval:60];
        [self StartMonitorNetworkStatus];
    }
    return self;
}
-(void)StartMonitorNetworkStatus{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if ([self->_delegate respondsToSelector:@selector(connectionStatusChanged:)]){
            [self->_delegate connectionStatusChanged:status];
        }
    }] ;
    [manager startMonitoring];
}
-(void)connectAPIWithType:(NSString*)Type Parameter:(nullable NSDictionary*)Parameter Method:(NSString*)method useToken:(BOOL)shouldToken
                  Success:(nullable void (^)(long StatusCode,NSString *Type,NSDictionary *Result))SuccessHandler
                     Fail:(nullable void (^)(long StatusCode,NSString *Message,NSDictionary *Result))FailHandler{
    
    self.AFSManager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableURLRequest *Request = [self.AFSManager.requestSerializer requestWithMethod:method URLString:[self getRequestURLwithType:Type] parameters:nil error:nil];
    Request.timeoutInterval = 120.0f;
    [Request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [Request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    if (shouldToken){
        [Request setValue:[self getAuthorization] forHTTPHeaderField:@"Authorization"];
    }
    if (Parameter){
        NSData *jsonRawData = [NSJSONSerialization dataWithJSONObject:Parameter options:NSJSONWritingPrettyPrinted error:nil];
        [Request setHTTPBody:jsonRawData];
    }
    self.Method = method;
    self.Type = Type;
    self.ConnectSuccessHandler = SuccessHandler;
    self.ConnectFailHandler = FailHandler;
    self.Request = Request;
    NSLog(@"Connect %@  %@ \n%@",self.Method,[self getRequestURLwithType:Type],Parameter);
    [self connectWithRequest];
}
//-(void)connectAPIWithImage:(UIImage*)image  Parameter:(nullable NSDictionary*)Parameter Type:(NSString*)Type
//                   Success:(nullable void (^)(long StatusCode,NSString *Type,NSDictionary *Result))SuccessHandler
//                      Fail:(nullable void (^)(long StatusCode,NSString *Message,NSDictionary *Result))FailHandler{
//
//    NSMutableURLRequest *Request =  [[AFJSONRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[self getRequestURLwithType:Type] parameters:Parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        [formData appendPartWithFileData:UIImageJPEGRepresentation(image,0.7)  name:@"file" fileName:@"image.jpg" mimeType:@"image/jpeg"];
//    } error:nil];
//
//    Request.timeoutInterval = 120.0f;
//    [Request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [Request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [Request setValue:[self getAuthorization] forHTTPHeaderField:@"Authorization"];
//
//    self.Method = @"POST";
//    self.Type = Type;
//    self.ConnectSuccessHandler = SuccessHandler;
//    self.ConnectFailHandler = FailHandler;
//    self.Request = Request;
//    NSLog(@"upload image %@ \n%@",Request.URL,Parameter);
//    [self connectWithRequest];
//}
-(void)connectWithRequest{
    if (self.Request){
        UIApplication *app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;
        NSURLSessionDataTask *task =  [self.AFSManager dataTaskWithRequest:self.Request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSDictionary *header = httpResponse.allHeaderFields;
            NSString *ServerLog = @"";
            if ([header objectForKey:@"Authorization"]){
                [self updateToken:[header objectForKey:@"Authorization"]];
            }
            self.Result = responseObject;
            if (!error){
                NSInteger statusCode = [httpResponse statusCode];
                NSLog(@"[%@] %@ %li",self.Method,self.Type,(long)statusCode);
                if (self.ConnectSuccessHandler){
                    self.ConnectSuccessHandler([httpResponse statusCode], self.Type, responseObject);
                }
            }else{
                NSInteger statusCode = (long)[[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                NSInteger operationCode = [error code];
                if ([responseObject objectForKey:@"error"]){
                    ServerLog = [[responseObject objectForKey:@"error"] objectForKey:@"message"];
                }
                NSLog(@"[%@] %@ %li 發生錯誤, Error %@ (%li)",self.Method,self.Type,(long)statusCode,[error localizedDescription],(long)operationCode);
                switch (statusCode) {
                    case 401:
                    {
                        if ([self->_delegate respondsToSelector:@selector(connectionUnAuth:)]){
                            [self->_delegate connectionUnAuth:self];
                        }
                    }
                        break;
                    case 0:
                    {
                        if (operationCode == -1001){
                            if ([self->_delegate respondsToSelector:@selector(connectionTimeout:)]){
                                [self->_delegate connectionTimeout:self];
                            }
                            
                        }else if (operationCode == -1009){
                            if ([self->_delegate respondsToSelector:@selector(connectionLost:)]){
                                [self->_delegate connectionLost:self];
                            }
                        }else{
                            ServerLog = [NSString stringWithFormat:@"%li,%@",(long)operationCode,[error localizedDescription]];
                            if (self.ConnectFailHandler){
                                self.ConnectFailHandler(statusCode,ServerLog, responseObject);
                            }
                        }
                    }
                        break;
                    default:
                    {
                        if (self.ConnectFailHandler){
                            self.ConnectFailHandler(statusCode,ServerLog, responseObject);
                        }
                    }
                        break;
                }
                
            }
            if ([ServerLog length]>0){
                 NSLog(@"ServerLog %@",ServerLog);
            }
           
            UIApplication *app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = NO;
        }];
        [task resume];
    }else{
        NSLog(@"沒有request");
    }
}
-(void)updateToken:(NSString *)token{
    if ([token length] > 0){
        NSArray *rawData = [token componentsSeparatedByString:@" "];
        NSLog(@"換新token啦 %@",rawData);
        if ([rawData count]> 1){
            AppDelegate *_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [_appDelegate.me updateUserAccessToken:[rawData objectAtIndex:1]];
        }
    }
}
-(void)reTryConnect{
    [self connectWithRequest];
}
#pragma mark Private Method
+(NSString*)getAPIURL{
    if ([self isTestServer]){
         return TEST_API_URL;
    }else{
         return API_URL;
    }
}
+(NSString*)getHostURL{
    if ([self isTestServer]){
        return TEST_HOST_URL;
    }else{
        return HOST_URL;
    }
}
+(BOOL)isTestServer{
    return YES;
    //return  [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"isTestServer"] boolValue];
}
-(NSString*)getRequestURLwithType:(NSString*)type{
    return  [NSString stringWithFormat:@"%@%@",[ConnectObj getAPIURL],type];
}
+(NSString*)DictionaryToURLParameter:(NSDictionary*)dic{
    NSMutableArray *queueArray = [[NSMutableArray alloc] init];
    for (NSString *key in [dic allKeys]) {
        [queueArray addObject: [NSString stringWithFormat:@"%@=%@",key,[dic objectForKey:key]]];
    }
    NSString *parameter = [queueArray componentsJoinedByString:@"&"];
    queueArray = nil;
    return parameter;
}
-(NSString *)getAuthorization{
    AppDelegate *_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    User *user = [_appDelegate.me getUser];
    if (user){
        return [NSString stringWithFormat:@"Bearer %@",user.accessToken];
    }else{
        return [NSString stringWithFormat:@"Bearer %@",_appDelegate.me.signupUser.accessToken];
    }
    
}
@end
