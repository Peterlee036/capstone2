//
//  User.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/7.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "User.h"
@implementation User
@synthesize ID,name,cellphone,email,remarks,IdentityStatus;
@synthesize token,firebaseToken,accessToken;
@synthesize certificationQRCODE,shareQRCODE,takeQRCODE,takePassword;
@synthesize placeName,placeAddress;

- (id) init{
    if (self = [super init]){
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}

-(void)getUserReady:(NSDictionary*)data{
    self.name = [data objectForKey:@"name"];
    self.cellphone = [data objectForKey:@"cellphone"];
    self.email = [data objectForKey:@"email"];
    self.remarks = [data objectForKey:@"remarks"];
    
    self.certificationQRCODE = [data objectForKey:@"certificationQRCODE"];
    self.takeQRCODE = [data objectForKey:@"takeQRCODE"];
    self.shareQRCODE = [data objectForKey:@"shareQRCODE"];
    
    self.placeName = [data objectForKey:@"placeName"];
    self.placeAddress = [data objectForKey:@"placeAddress"];
    
    if ([[data objectForKey:@"IdentityStatus"] isEqualToString:@"leader"]){
        self.IdentityStatus = [NSNumber numberWithInt:IdentityTypeLeader];
    }else if ([[data objectForKey:@"IdentityStatus"] isEqualToString:@"resident"]){
        self.IdentityStatus = [NSNumber numberWithInt:IdentityTypeResident];
    }else{
        self.IdentityStatus = [NSNumber numberWithInt:IdentityTypeUnknow];
    }
    
    if ([data objectForKey:@"takePassword"]){
        NSLog(@"為甚麼會進來改密碼");
        self.takePassword = [data objectForKey:@"takePassword"];
    }
}
-(void)updateUser:(NSDictionary*)data{
    self.name = [data objectForKey:@"name"];
    self.cellphone = [data objectForKey:@"cellphone"];
    self.email = [data objectForKey:@"email"];
    if ([data objectForKey:@"takePassword"]){
        self.takePassword = [data objectForKey:@"takePassword"];
    }
    self.firebaseToken = [data objectForKey:@"firebaseToken"];
}
- (id)copyWithZone:(NSZone *)zone
{
    User *_user = [[User alloc] init];
    _user.ID  = self.ID;
    _user.name = self.name;
    _user.cellphone =  self.cellphone;
    _user.email = self.email;
    _user.remarks = self.remarks;
    _user.certificationQRCODE = self.certificationQRCODE;
    _user.shareQRCODE = self.shareQRCODE;
    _user.takeQRCODE = self.takeQRCODE;
    _user.placeName = self.placeName;
    _user.placeAddress = self.placeAddress;
    _user.IdentityStatus = self.IdentityStatus;
    _user.accessToken = self.accessToken;
    
    return _user;
}

//-(void)updateShareQRCodeWithString:(NSString*)string{
//    self.shareQRCODE = string;
//    self.ShareQRCodeImage = [Tools GeneratorQRCodeFromString:string size:250.f];
//}
//-(void)updateTakeQRCodeWithString:(NSString*)string{
//    self.takeQRCODE = string;
//    self.TakeQRCodeImage = [Tools GeneratorQRCodeFromString:string size:250.f];
//    NSLog(@"TakeQRCodeImage %@",self.TakeQRCodeImage);
//}
//-(void)updateCertQRCodeWithString:(NSString*)string{
//    self.certificationQRCODE = string;
//    self.CertQRCodeImage = [Tools GeneratorQRCodeFromString:string size:250.f];
//}
@end
