//
//  Call.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>


@interface Call : RLMObject{
    NSDateFormatter *dateformatter;
}
@property NSString *ID;
@property NSString *customerID;
@property NSString *placeID;
@property NSString *householdID;
@property NSDate *start_at;
@property NSDate *end_at;
@property NSString *during;
@property NSString *status;
@property NSString *callPeople;
@property NSString *callRoom;
@property NSString *action;
@property NSString *callResidentID;
-(void)getCallReady:(NSDictionary*)data;
-(NSString *)getTimestampString;
@end

