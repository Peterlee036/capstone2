//
//  Notify.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "Notify.h"

@implementation Notify
- (id) init{
    if (self = [super init]){
        NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
        NSTimeZone *dateTimezone =  [NSTimeZone timeZoneWithName:@"Asia/Taipei"];
        dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setLocale:datelocale];
        [dateformatter setTimeZone:dateTimezone];
        [dateformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    }
    return self;
}
+(NSString *)primaryKey{
    return @"ID";
}
+ (NSDictionary *)defaultPropertyValues {
    return @{@"ActionType" : [NSNumber numberWithInteger:NotifyActionTypeUnknow],
             @"PeopleType":  [NSNumber numberWithInteger:NotifyPeopleTypeUnknow],
             @"isReady":[NSNumber numberWithBool:NO]
    };
}
-(void)getNotifyReady:(NSDictionary*)data{
    self.ID = [data objectForKey:@"noticeNo"];
    
    if ([[data objectForKey:@"action"] isEqualToString:@"package"]){
        self.ActionType = [NSNumber numberWithInteger:NotifyActionTyePackage];
    }else if ([[data objectForKey:@"action"] isEqualToString:@"take"]){
        self.ActionType = [NSNumber numberWithInteger:NotifyActionTypeTake];
    }else{
         self.ActionType = [NSNumber numberWithInteger:NotifyActionTypeUnknow];
    }
    
    if ([[data objectForKey:@"peopleType"] isEqualToString:@"postman"]){
        self.name = @"郵差";
        self.ActionType = [NSNumber numberWithInteger:NotifyPeoplePostMan];
    }else if ([[data objectForKey:@"peopleType"] isEqualToString:@"logistics"]){
         self.name = @"物流士";
        self.ActionType = [NSNumber numberWithInteger:NotifyPeopleLogistics];
    }else{
         self.name = @"";
         self.ActionType = [NSNumber numberWithInteger:NotifyPeopleTypeUnknow];
    }
    
    if ([[data objectForKey:@"putDate"] length] > 0){
        self.put_at = [dateformatter dateFromString:[data objectForKey:@"putDate"]];
    }
    
    if ([[data objectForKey:@"created_at"] length] > 0){
        self.create_at = [dateformatter dateFromString:[data objectForKey:@"created_at"]];
    }
}
-(NSString *)getTimestampString:(NSDate*)date{
    if (date){
        NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
        NSTimeZone *dateTimezone =  [NSTimeZone timeZoneWithName:@"Asia/Taipei"];
        NSDateFormatter *output_dateformatter = [[NSDateFormatter alloc] init];
        [output_dateformatter setLocale:datelocale];
        [output_dateformatter setTimeZone:dateTimezone];
        [output_dateformatter setDateFormat:@"YYYY.MM.dd HH:mm"];
        return [output_dateformatter stringFromDate:date];
    }else{
        return @"";
    }
}
-(NSString*)getLastingTime{
    if (self.put_at){
        int LastingTime;
        LastingTime = round([[NSDate date] timeIntervalSinceDate:self.put_at]);
        int day = LastingTime / 86400;
        int hour = (LastingTime % 86400) / 3600;
        return [NSString stringWithFormat:@"%i天%i小時",day,hour];
    }else{
        return @"";
    }
}
@end
