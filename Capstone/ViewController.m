//
//  ViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/6.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ViewController.h"
#import "MainTabbarController.h"
#import "Signup/QRCodeScannerViewController.h"
#import "Signup/NotSignupViewController.h"
@interface ViewController (){
    AppDelegate *delegate;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.navigationController setNavigationBarHidden:YES];
    
    ScanBtn.layer.cornerRadius = 7.0f;
    NextBtn.layer.cornerRadius = 7.0f;
    NextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    NextBtn.layer.borderWidth = 1.0f;
    
    delegate.me = [[UsersRepository alloc] init];
    delegate.me.Realm = delegate.DefaultRealm;
    [delegate.me login];
}
-(void)viewWillAppear:(BOOL)animated{
    if (delegate.me.LoginStatus == UserLoginStatusLogin){
        [self goMainView:nil];
    }
}
-(IBAction)goScanner:(id)sender{
    QRCodeScannerViewController *QRCodeScannerView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"QRCodeScannerView"];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:QRCodeScannerView];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}
-(IBAction)byPass:(id)sender{
    NotSignupViewController *NotSignupView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotSignupView"];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:NotSignupView];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}
-(void)goMainView:(id)sender{
    MainTabbarController *MainTabbarView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"MainTabbarView"];
    MainTabbarView.defaultIndexPage = 2;
    [self.navigationController pushViewController:MainTabbarView animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
@end
