//
//  PackageViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "PackageViewController.h"
#import "../TableViewCell/TransparentHeader.h"
#import "../TableViewCell/PackageHeaderTableViewCell.h"
#import "../TableViewCell/PackageInfoTableViewCell.h"
#import "ShowQRCodeViewController.h"
#import "PackageRecordViewController.h"
#import "NotifyViewController.h"
@interface PackageViewController (){
    AppDelegate *delegate;
}

@end

@implementation PackageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    PackageRepo = [[PackageRepository alloc] init];
    PackageRepo.Realm = delegate.DefaultRealm;
    [PackageRepo getPackageFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self->mainTableview reloadData];
        [self->mainTableview setContentOffset:CGPointZero animated:YES];
    }];
    mainTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    mainTableview.separatorColor = [UIColor clearColor];
}
-(void)viewWillAppear:(BOOL)animated{
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger numberOfrow = PackageRepo.Result.count;
    if (numberOfrow == 0 && section == 1){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"查詢結果沒有資料";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        return 0;
    }else{
        tableView.backgroundView = nil;
        if (section == 0){
            return 1;
        }else{
           return numberOfrow;
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell;
    NSString *CellIdentifier;
    if ([indexPath section] == 0){
        CellIdentifier = @"PackageHeader";
        PackageHeaderTableViewCell *PackageHeader = (PackageHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (PackageHeader == nil) {
            PackageHeader = [[PackageHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [PackageHeader.PageageBtn1 addTarget:self action:@selector(goTakeQRCode) forControlEvents:UIControlEventTouchUpInside];
        [PackageHeader.PageageBtn2 addTarget:self action:@selector(goPackageRecord) forControlEvents:UIControlEventTouchUpInside];
        cell = PackageHeader;
        return cell;
    }else{
        NSString *CellIdentifier = @"PackageInfoCell";
        PackageInfoTableViewCell *PackageInfoCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (PackageInfoCell == nil) {
            PackageInfoCell = [[PackageInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        Package *PackageObj = [PackageRepo.Result objectAtIndex:[indexPath row]];
        
        PackageInfoCell.title.text = PackageObj.type;
        PackageInfoCell.put_at.text = [NSString stringWithFormat:@"投遞時間:%@",[PackageObj getTimestampString:PackageObj.put_at]];
        PackageInfoCell.duration.text = [NSString stringWithFormat:@"滯留時間:%@",[PackageObj getLastingTime]];
        PackageInfoCell.serial.text =PackageObj.status;
         cell = PackageInfoCell;
        return cell;
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 0){
        return 80;
    }else{
       return 100;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1 && PackageRepo.Result.count != 0){
        return 55;
    }else{
        return 0;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1 ){
        static NSString *CellIdentifier = @"TransparentHeaderCell";
        TransparentHeader *TransparentHeaderCell = (TransparentHeader *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        TransparentHeaderCell.backgroundColor = [UIColor whiteColor];
        
        if (TransparentHeaderCell == nil) {
            TransparentHeaderCell = [[TransparentHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        TransparentHeaderCell.title.text = @"待領包裹";
        return TransparentHeaderCell;
    }else{
        return nil;
    }
}


#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self.tabBarController presentViewController:NotifyView animated:YES completion:nil];
}
-(void)goTakeQRCode{
    ShowQRCodeViewController *ShowQRCodeView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ShowQRCodeView"];
    ShowQRCodeView.qrcodeType = QRCodeTypeTake;
    User *user = [delegate.me getUser];
    ShowQRCodeView.qrcodeStr = user.takeQRCODE;
    [self.tabBarController presentViewController:ShowQRCodeView animated:YES completion:nil];
}
-(void)goPackageRecord{
    PackageRecordViewController *PackageRecordView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"PackageRecordView"];
    [self.navigationController pushViewController:PackageRecordView animated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end

