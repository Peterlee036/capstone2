//
//  ModifyUserViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/21.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ModifyUserViewController.h"
#import "SCLAlertView.h"
#import "../TableViewCell/TextfieldTableViewCell.h"
#import "../TableViewCell/SectionHeaderTableViewCell.h"
#import "../TableViewCell/TransparentHeader.h"
#import "NotifyViewController.h"
@interface ModifyUserViewController (){
    AppDelegate *delegate;
}
@end

@implementation ModifyUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    mainView.layer.cornerRadius = 15;
    user = [delegate.me getUser];
    modifyTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    submitBtn.layer.cornerRadius = 5;
    UITapGestureRecognizer *singleTaprecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(handleTapGesture:)];
    [singleTaprecognizer setNumberOfTouchesRequired : 1];
    [self.view addGestureRecognizer : singleTaprecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHiden:) name:UIKeyboardWillHideNotification object:nil];

    [self initDataSource];
}
-(void)viewWillAppear:(BOOL)animated{
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark - DataSource Method
-(void)initDataSource{
    SectionName = @[@"修改個人資料",@"修改取貨的開櫃密碼"];
    NSArray *tempDataSource = @[@[@{@"title":@"姓名",@"value":[NSString stringWithFormat:@"%@",user.name],@"key":@"name",@"ModifyFieldType":[NSNumber numberWithInt:ModifyFieldTypeName]},
                     @{@"title":@"手機",@"value":[NSString stringWithFormat:@"%@",user.cellphone],@"key":@"cellphone",@"ModifyFieldType":[NSNumber numberWithInt:ModifyFieldTypeCellPhone]},
                     @{@"title":@"E-mail",@"value":user.email,@"key":@"email",@"ModifyFieldType":[NSNumber numberWithInt:ModifyFieldTypeEmail]}],
                   @[@{@"title":@"請輸入密碼",@"value":[NSString stringWithFormat:@"%@",user.takePassword],@"key":@"takePassword",@"ModifyFieldType":[NSNumber numberWithInt:ModifyFieldTypePassword]},
                     @{@"title":@"再次輸入密碼",@"value":[NSString stringWithFormat:@"%@",user.takePassword],@"key":@"takePassword",@"ModifyFieldType":[NSNumber numberWithInt:ModifyFieldTypePasswordConfirm]}]];
    DataSource = [tempDataSource mutableCopy];
    
    FieldIndex = [[NSMutableDictionary alloc] init];
    for (int s = 0;s<[DataSource count];s++){
        NSArray *data = [DataSource objectAtIndex:s];
        for (int i = 0;i<[data count];i++){
            NSNumber *key =  [[data objectAtIndex:i] objectForKey:@"ModifyFieldType"];
            NSIndexPath *rowIndex = [NSIndexPath indexPathForRow:i inSection:s];
            [FieldIndex setObject:rowIndex forKey:key];
        }
    }
}
-(NSIndexPath*)getRowIndexByFieldType:(ModifyFieldType)type{
    return  [FieldIndex objectForKey:[NSNumber numberWithInteger:type]] ;
}
-(NSDictionary *)getRowDatawithIndexPath:(NSIndexPath*)indexpath{
    NSDictionary *RowData = [[DataSource objectAtIndex:[indexpath section]] objectAtIndex:[indexpath row]];
    return RowData;
}
-(void)updateDataSourceWithFieldType:(ModifyFieldType)type Value:(id)value{
    NSIndexPath *rowIndex = [self getRowIndexByFieldType:type];
    NSMutableDictionary *MutableRowData = [[self getRowDatawithIndexPath:rowIndex] mutableCopy];
    [MutableRowData setObject:value forKey:@"value"];
    NSMutableArray *SectionArray = [[DataSource objectAtIndex:[rowIndex section]] mutableCopy];
    [SectionArray replaceObjectAtIndex:[rowIndex row] withObject:MutableRowData];
    [DataSource replaceObjectAtIndex:[rowIndex section] withObject:SectionArray];
}
#pragma mark TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [DataSource count];;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[DataSource objectAtIndex:section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TextFieldCell";
    TextfieldTableViewCell *cell = (TextfieldTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TextfieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *rowData = [[DataSource objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
    int fieldType = [[rowData objectForKey:@"ModifyFieldType"] intValue];
    switch (fieldType) {
        case ModifyFieldTypePassword:
        case ModifyFieldTypePasswordConfirm:
            cell.textfield.secureTextEntry = YES;
            break;
        default:
            cell.textfield.secureTextEntry = NO;
            break;
    }
    cell.title.text = [rowData objectForKey:@"title"];
    cell.textfield.text = [rowData objectForKey:@"value"];
    cell.textfield.placeholder = [NSString stringWithFormat:@"請輸入您的%@",[rowData objectForKey:@"title"]];
    cell.textfield.tag = fieldType;
    cell.textfield.delegate = self;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
 
        static NSString *CellIdentifier = @"SectionHeader";
        SectionHeaderTableViewCell *SectionCell = (SectionHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        SectionCell.backgroundColor = [UIColor whiteColor];
        
        if (SectionCell == nil) {
            SectionCell = [[SectionHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        SectionCell.title.text = [SectionName objectAtIndex:section];
        return SectionCell;
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}
#pragma mark - UITextField Method
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == ModifyFieldTypeCellPhone){
        //電話
        NSCharacterSet *acceptedInput = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        if ((([string rangeOfCharacterFromSet:acceptedInput].location == NSNotFound) && [textField.text length] <= 9) || [string isEqualToString:@""]){
            return  YES;
        }else{
            return NO;
        }
    }else{
        return YES;
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    firstResponse = textField;
    switch (textField.tag) {
        case ModifyFieldTypePassword:
            //密碼
        case ModifyFieldTypePasswordConfirm:
            //確認密碼
            textField.text = @"";
            textField.secureTextEntry = YES;
            break;

        case ModifyFieldTypeCellPhone:
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.secureTextEntry = NO;
            textField.inputView = nil;
            textField.inputAccessoryView = nil;
            break;
        default:
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.secureTextEntry = NO;
            textField.inputView = nil;
            textField.inputAccessoryView = nil;
            break;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField.text length] > 0){
        switch (textField.tag) {
            case ModifyFieldTypeName:
                //名稱
                if ([textField.text length] == 0){
                      [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"名稱為必填欄位"];
                    
                }else{
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }
                break;
        
            case ModifyFieldTypeEmail:
                //email
                
                if ([Tools isEmailFormat:textField.text]){
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }else{
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"輸入的email不是正確格式"];
                }
                
                break;
            case ModifyFieldTypeCellPhone:
                //電話
                if ([Tools ifPhoneNumberCharacterSet:textField.text]){
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }else{
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"電話號碼需為10碼數字，不包含特殊符號"];
                }
                break;
            case ModifyFieldTypePassword:
                //密碼
            {
                if ([textField.text length] < 6){
                   [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"密碼須大於或等於6碼"];
                }else{
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }
            }
                break;
            case ModifyFieldTypePasswordConfirm:
                //確認密碼
            {
              if ([textField.text length] < 6){
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"密碼須大於或等於6碼"];
                }else{
                   [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }
            }
                break;
         
            default:
            {
                [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
            }
                break;
        }
    }
}

#pragma mark - Commend Method
-(void)ShowErrorAlertWithTitle:(NSString*)title Message:(NSString*)msg {
    UIColor *tinColor = [UIColor colorWithRed:253/255.0f green:137/255.0f blue:45/255.0f alpha:1.0];
    SCLAlertView *SCLAlert = [[SCLAlertView alloc] initWithNewWindowWidth:self.view.frame.size.width*0.8];
    SCLAlert.backgroundType = SCLAlertViewBackgroundBlur;
    SCLAlert.customViewColor = tinColor;
    SCLAlert.shouldDismissOnTapOutside = YES;
    SCLAlert.horizontalButtons = YES;
    [SCLAlert showInfo:title subTitle:msg closeButtonTitle:@"再試一次" duration:0.0f];
}
-(BOOL)getVerifyUserData{
    BOOL Verified = YES;
    NSString *errorMessgae;
    for (int s = 0;s<[DataSource count];s++){
        NSMutableArray *data = [DataSource objectAtIndex:s];
        for (int i = 0;i<[data count];i++){
            NSMutableDictionary *UserData = [data objectAtIndex:i];
            NSNumber *key =  [UserData objectForKey:@"ModifyFieldType"];
            switch ([key intValue]) {
                case ModifyFieldTypeName:
                    //必填欄位
                    if ([[UserData objectForKey:@"value"] length] == 0){
                        errorMessgae = [NSString stringWithFormat:@"%@欄位為必填",[UserData objectForKey:@"display_name"]];
                        Verified = NO;
                    }
                    break;
                case ModifyFieldTypeCellPhone:
                    if (![Tools ifPhoneNumberCharacterSet:[UserData objectForKey:@"value"]]){
                        errorMessgae = @"電話號碼需為10碼數字，不包含特殊符號";
                        Verified = NO;
                    }
                    break;
                case ModifyFieldTypeEmail:
                    if (![Tools isEmailFormat:[UserData objectForKey:@"value"]] || [[UserData objectForKey:@"value"] isEqualToString:@""] ){
                        errorMessgae = @"email 格式不正確";
                        Verified = NO;
                    }
                    break;
                case ModifyFieldTypePassword:
                    //password
                {
                    NSIndexPath  *indexRow = [self getRowIndexByFieldType:ModifyFieldTypePassword];
                    NSDictionary *otherRow = [self getRowDatawithIndexPath:indexRow];
                    if (![[UserData objectForKey:@"value"] isEqualToString:[otherRow objectForKey:@"value"]] || [[UserData objectForKey:@"value"] length] < 6  ){
                        Verified = NO;
                        errorMessgae = @"密碼驗證錯誤";
                    }
                    
                }
                    break;
                default:
                    break;
            }

        }
    }
    if (!Verified){
         [self ShowErrorAlertWithTitle:@"修改資料錯誤" Message:errorMessgae];
    }
    
    return Verified;
}
#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self presentViewController:NotifyView animated:YES completion:nil];
}
-(IBAction)goModityUser:(id)sender{
     [self.view endEditing:YES];
    if ([self getVerifyUserData]){
        NSMutableDictionary *parameter  = [[NSMutableDictionary alloc] init];
        for (NSNumber *key in [FieldIndex allKeys]){
            NSIndexPath *index = [FieldIndex objectForKey:key];
            NSDictionary *row = [[DataSource objectAtIndex:[index section]] objectAtIndex:[index row]];
            [parameter setObject:[row objectForKey:@"value"] forKey:[row objectForKey:@"key"]];
            
        }
        [delegate.me updateUserFromAPI:parameter completion:^(BOOL Successful, long StatusCode, NSString *Message) {
            SCLAlertView *SCLAlert = [[SCLAlertView alloc] initWithNewWindow];
                        SCLAlert.backgroundType = SCLAlertViewBackgroundBlur;
                        SCLAlert.customViewColor = [UIColor colorWithRed:101/255.0f green:182/255.0f blue:88/255.0f alpha:1];
                        SCLAlert.shouldDismissOnTapOutside = NO;
                        //SCLAlert.horizontalButtons = YES;
                            SCLButton *SubmitBtn = [SCLAlert addButton:@"確認" actionBlock:^{
                                [self.navigationController popViewControllerAnimated:YES];
                            }];
                            SubmitBtn.buttonFormatBlock = ^NSDictionary *{
                                NSMutableDictionary *btnConfig = [[NSMutableDictionary alloc] init];
                                btnConfig[@"backgroundColor"] = [UIColor colorWithRed:0/255.0f green:147/255.0f blue:229/255.0f alpha:1];;
                                btnConfig[@"textColor"] =[UIColor whiteColor];
                                return  btnConfig;
                            };
                        [SCLAlert showSuccess:@"儲存成功！" subTitle:@"您的資料已更新！" closeButtonTitle:nil duration:0.0f];
        }];


    }
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Keyboard Delegate
-(void)keyboardWillShow:(NSNotification*)notification {
    NSIndexPath *row = [self getRowIndexByFieldType:firstResponse.tag];
    if ([row section] > 0){
        float newY =  200;
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.view setFrame:CGRectMake(0, 0-newY, self.view.frame.size.width, self.view.frame.size.height)];
        } completion:nil];
    }
}
-(void)keyboardWillHiden:(NSNotification*)notification {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } completion:nil];
}
-(void)handleTapGesture:(UITapGestureRecognizer *) gestureRecognizer{
    [self.view endEditing:YES];
}
-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end
