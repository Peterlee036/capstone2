//
//  HomeViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/17.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
#import "../Repository/CallRepository.h"
#import "../Repository/PackageRepository.h"
#import "../Repository/NotifyRepository.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    NSArray *datasource;
    IBOutlet UITableView *mainTableview;
    IBOutlet UIButton *nBadge;
    User *user;
    CallRepository *CallRepo;
    PackageRepository *PackageRepo;
    NotifyRepository *NotifyRepo;
}

@end

NS_ASSUME_NONNULL_END
