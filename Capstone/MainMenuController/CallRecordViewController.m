//
//  CallRecordViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "CallRecordViewController.h"
#import "../TableViewCell/TransparentHeader.h"
#import "../TableViewCell/CallTableViewCell.h"
#import "NotifyViewController.h"
@interface CallRecordViewController (){
    AppDelegate *delegate;
}
@end

@implementation CallRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    CallRepo = [[CallRepository alloc] init];
    CallRepo.Realm = delegate.DefaultRealm;
    [CallRepo getCallFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self->mainTableview reloadData];
        [self->mainTableview setContentOffset:CGPointZero animated:YES];
    }];
    mainTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)viewWillAppear:(BOOL)animated{
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger numberOfrow = CallRepo.Result.count;
    if (numberOfrow == 0){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"查詢結果沒有資料";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
    }else{
        tableView.backgroundView = nil;
    }
    return numberOfrow;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"CallCell";
    CallTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CallTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Call *callObj = [CallRepo.Result objectAtIndex:[indexPath row]];
    cell.name.text = callObj.callPeople;
    [cell setStatusCalled:[callObj.status isEqualToString:@"called"]];
    cell.datetime.text = [callObj getTimestampString];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   return 80;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
     return 55;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
     return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *CellIdentifier = @"TransparentHeaderCell";
    TransparentHeader *TransparentHeaderCell = (TransparentHeader *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    TransparentHeaderCell.backgroundColor = [UIColor whiteColor];
    
    if (TransparentHeaderCell == nil) {
        TransparentHeaderCell = [[TransparentHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    TransparentHeaderCell.title.text = @"通話紀錄";
    return TransparentHeaderCell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
    UIView *backgroundview = [[UIView alloc] initWithFrame:Rect];
    backgroundview.backgroundColor = [UIColor whiteColor];
    [cell addSubview:backgroundview];
    cell.backgroundColor = [UIColor colorWithRed:88/255.0f green:125/255.0f blue:139/255.0f alpha:1];
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:backgroundview.bounds
                              byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(15, 15) ];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = backgroundview.bounds;
    maskLayer.path = maskPath.CGPath;
    backgroundview.layer.mask = maskLayer;
    return cell;
}
#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self.tabBarController presentViewController:NotifyView animated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
