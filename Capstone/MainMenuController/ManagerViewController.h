//
//  ManagerViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
#import "../Repository/ManagerRepository.h"
NS_ASSUME_NONNULL_BEGIN

@interface ManagerViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    NSArray *datasource;
    IBOutlet UITableView *mainTableview;
    ManagerRepository *ManagerRepo;
    IBOutlet UIActivityIndicatorView *loadingView;
     IBOutlet UIButton *nBadge;
}

@end

NS_ASSUME_NONNULL_END
