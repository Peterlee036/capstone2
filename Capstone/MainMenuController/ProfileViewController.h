//
//  ProfileViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate>{
    NSArray *datasource;
    IBOutlet UITableView *mainTableview;
    User *user;
     IBOutlet UIButton *nBadge;
}
@end

NS_ASSUME_NONNULL_END
