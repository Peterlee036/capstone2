//
//  NotifyViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "../AppDelegate.h"
#import "../Repository/NotifyRepository.h"

@interface NotifyViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    IBOutlet UITableView *mainTableview;
    NotifyRepository *NotifyRepo;
     IBOutlet UIButton *nBadge;
}

@end

