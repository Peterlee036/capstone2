//
//  ShowQRCodeViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Tools.h"
typedef NS_ENUM(NSUInteger, QRCodeType) {
    QRCodeTypeCert = 0,
    QRCodeTypeShare,
    QRCodeTypeTake
};
@interface ShowQRCodeViewController : UIViewController{
    IBOutlet UIView *bgview;
    IBOutlet UIImageView *qrcodeImageView;
    IBOutlet UILabel *headerLabel;
    IBOutlet UIView *detailView;
    IBOutlet UILabel *describe;
    NSArray *headerArray;
    NSArray *describeArray;
}

@property QRCodeType qrcodeType;
@property(strong,nonatomic) NSString *qrcodeStr;

@end

