//
//  ResidentViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
#import "../Repository/ResidentRepository.h"
@interface ResidentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    IBOutlet UITableView *mainTableview;
      ResidentRepository *ResidentRepo;
     IBOutlet UIButton *nBadge;
}

@end
