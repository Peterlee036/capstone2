//
//  NotifyViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "NotifyViewController.h"
#import "../TableViewCell/TransparentHeader.h"
#import "../TableViewCell/NotifyTableViewCell.h"

@interface NotifyViewController (){
    AppDelegate *delegate;
}

@end

@implementation NotifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NotifyRepo = [[NotifyRepository alloc] init];
    NotifyRepo.Realm = delegate.DefaultRealm;
    [NotifyRepo getNotiyFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self->mainTableview reloadData];
        [self->mainTableview setContentOffset:CGPointZero animated:YES];
        
    }];
    mainTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger numberOfrow = NotifyRepo.Result.count;
    if (numberOfrow == 0){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"查詢結果沒有資料";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
    }else{
        tableView.backgroundView = nil;
    }
    return numberOfrow;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"NotifyCell";
    NotifyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NotifyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Notify *notifyObj = [NotifyRepo.Result objectAtIndex:[indexPath row]];
   
    switch ([notifyObj.ActionType intValue]) {
        case NotifyActionTypeTake:
        {
            //滯留包裹
            cell.title.text = @"領取包裹・提醒";
            cell.datetime.text = [notifyObj getTimestampString:notifyObj.create_at];
            cell.desc.text = [NSString stringWithFormat:@"您有一個包裹尚未領取，於%@由%@投遞，目前滯留時間：%@，請盡快前往實體櫃領取包裹。",[notifyObj getTimestampString:notifyObj.put_at],notifyObj.name,[notifyObj getLastingTime]];
        }
            break;
        case NotifyActionTyePackage:
        {
            //成功領取
            cell.title.text = @"包裹・投遞成功";
            cell.datetime.text = [notifyObj getTimestampString:notifyObj.create_at];
            cell.desc.text = [NSString stringWithFormat:@"您有一個包裹於%@由%@投遞成功，您可前往實體櫃領取包裹。",[notifyObj getTimestampString:notifyObj.put_at],notifyObj.name];
        }
            break;
        default:
            break;
    }
    
    //cell.datetime.text = [callObj getTimestampString];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
     return 55;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
     return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *CellIdentifier = @"TransparentHeaderCell";
    TransparentHeader *TransparentHeaderCell = (TransparentHeader *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    TransparentHeaderCell.backgroundColor = [UIColor whiteColor];
    
    if (TransparentHeaderCell == nil) {
        TransparentHeaderCell = [[TransparentHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    TransparentHeaderCell.title.text = @"最新通知";
    return TransparentHeaderCell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
    UIView *backgroundview = [[UIView alloc] initWithFrame:Rect];
    backgroundview.backgroundColor = [UIColor whiteColor];
    [cell addSubview:backgroundview];
    cell.backgroundColor = [UIColor colorWithRed:88/255.0f green:125/255.0f blue:139/255.0f alpha:1];
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:backgroundview.bounds
                              byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(15, 15) ];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = backgroundview.bounds;
    maskLayer.path = maskPath.CGPath;
    backgroundview.layer.mask = maskLayer;
    return cell;
}
#pragma mark IBAction Method
-(IBAction)goClose:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
