//
//  PackageRecordViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/20.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "PackageRecordViewController.h"
#import "SCLAlertView.h"
#import "../TableViewCell/TransparentHeader.h"
#import "../TableViewCell/PackageInfoTableViewCell.h"
#import "NotifyViewController.h"
@interface PackageRecordViewController (){
    AppDelegate *delegate;
}


@end

@implementation PackageRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    PackageRepo = [[PackageRepository alloc] init];
    PackageRepo.Realm = delegate.DefaultRealm;
    [PackageRepo getPackageTaked];
    mainTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)viewWillAppear:(BOOL)animated{
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger numberOfrow = PackageRepo.Result.count;
    if (numberOfrow == 0){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"查詢結果沒有資料";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }else{
        tableView.backgroundView = nil;
    }
    return numberOfrow;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"PackageInfoCell";
    PackageInfoTableViewCell *PackageInfoCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (PackageInfoCell == nil) {
        PackageInfoCell = [[PackageInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Package *PackageObj = [PackageRepo.Result objectAtIndex:[indexPath row]];
    PackageInfoCell.title.text = PackageObj.type;
    PackageInfoCell.put_at.text = [NSString stringWithFormat:@"投遞時間:%@",[PackageObj getTimestampString:PackageObj.put_at]];
    PackageInfoCell.duration.text = [NSString stringWithFormat:@"領取時間:%@",[PackageObj getTimestampString:PackageObj.take_at]];
    PackageInfoCell.serial.text =PackageObj.status;
   // [cell.deleteBtn addTarget:self action:@selector(deleteResideBtn:) forControlEvents:UIControlEventTouchUpInside];
    return PackageInfoCell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (PackageRepo.Result.count != 0){
          return 55;
    }else{
        return 0;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *CellIdentifier = @"TransparentHeaderCell";
    TransparentHeader *TransparentHeaderCell = (TransparentHeader *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    TransparentHeaderCell.backgroundColor = [UIColor whiteColor];
    
    if (TransparentHeaderCell == nil) {
        TransparentHeaderCell = [[TransparentHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [TransparentHeaderCell.bgView setHidden:YES];
    TransparentHeaderCell.title.text = @"包裹查詢";
    return TransparentHeaderCell;
    
}

#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self presentViewController:NotifyView animated:YES completion:nil];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
