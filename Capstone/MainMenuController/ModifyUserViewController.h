//
//  ModifyUserViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/21.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, ModifyFieldType) {
    ModifyFieldTypeName = 0,
    ModifyFieldTypeCellPhone,
    ModifyFieldTypeEmail,
    ModifyFieldTypePassword,
    ModifyFieldTypePasswordConfirm
};
@interface ModifyUserViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    IBOutlet UIView *mainView;
    IBOutlet UIButton *submitBtn;
    User *user;
    NSArray *SectionName;
    NSMutableArray *DataSource;
    NSMutableDictionary *FieldIndex;
    IBOutlet UITableView *modifyTable;
    UITextField *firstResponse;
     IBOutlet UIButton *nBadge;
}
@end

NS_ASSUME_NONNULL_END
