//
//  HomeViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/17.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "HomeViewController.h"
#import "../TableViewCell/TransparentHeader.h"
#import "../TableViewCell/PackageBadgeTableViewCell.h"
#import "../TableViewCell/ProfileTableViewCell.h"
#import "../TableViewCell/QRcodeTableViewCell.h"
#import "../TableViewCell/CallTableViewCell.h"
#import "ShowQRCodeViewController.h"
#import "ResidentViewController.h"
#import "NotifyViewController.h"
@interface HomeViewController (){
    AppDelegate *delegate;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    CallRepo = [[CallRepository alloc] init];
    CallRepo.Realm = delegate.DefaultRealm;
    [CallRepo getCallFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self->mainTableview reloadData];
        [self->mainTableview setContentOffset:CGPointZero animated:YES];
    }];
    
    PackageRepo = [[PackageRepository alloc] init];
    PackageRepo.Realm = delegate.DefaultRealm;
    [PackageRepo getPackageFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self->mainTableview reloadData];
        [self->mainTableview setContentOffset:CGPointZero animated:YES];
    }];
    
    NotifyRepo = [[NotifyRepository alloc] init];
    NotifyRepo.Realm = delegate.DefaultRealm;
    [NotifyRepo getNotiyFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        self->delegate.notifyBadge = [NSNumber numberWithInteger: self->NotifyRepo.Result.count];
        [self initNotifyBadge];
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    user = [delegate.me getUser];
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
        case 2:
            return 1;
            break;
        case 3:
            return [CallRepo.Result count];
        default:
            return 0;
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier;
    id cell;
    switch ([indexPath section]) {
        case 0:
        {
            CellIdentifier = @"ProfileCell";
            ProfileTableViewCell *ProfileCell = (ProfileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (ProfileCell == nil) {
                ProfileCell = [[ProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            [ProfileCell.func1Btn addTarget:self action:@selector(goShareQRCode) forControlEvents:UIControlEventTouchUpInside];
            [ProfileCell.func2Btn addTarget:self action:@selector(goAccountView) forControlEvents:UIControlEventTouchUpInside];
            
            ProfileCell.name.text = user.name;
            ProfileCell.address.text = user.placeName;
            ProfileCell.address_detail.text = user.placeAddress;
            ProfileCell.note.text = user.remarks;
            cell = ProfileCell;
        }
            return cell;
            break;
        case 1:
            
        {
            CellIdentifier = @"QRCodeCell";
            QRcodeTableViewCell *QRCodeCell = (QRcodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (QRCodeCell == nil) {
                QRCodeCell = [[QRcodeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            UIImage *qrcode = [Tools GeneratorQRCodeFromString:user.takeQRCODE size:150.0f];
            [QRCodeCell.QRcodeImage setImage:qrcode];
            
            cell = QRCodeCell;
        }
            return cell;
            break;
        case 2:
            
        {
            CellIdentifier = @"PackageBadgeCell";
            PackageBadgeTableViewCell *PackageBadgeCell = (PackageBadgeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (PackageBadgeCell == nil) {
                PackageBadgeCell = [[PackageBadgeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            PackageBadgeCell.badgeNumer.text = [NSString stringWithFormat:@"%li",(long)PackageRepo.Result.count];
            cell = PackageBadgeCell;
        }
            return cell;
            break;
        default:
        case 3:
        {
            CellIdentifier = @"CallCell";
            CallTableViewCell *CallCell = (CallTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (CallCell == nil) {
                CallCell = [[CallTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            Call *callObj = [CallRepo.Result objectAtIndex:[indexPath row]];
            CallCell.name.text = callObj.callPeople;
            [CallCell setStatusCalled:[callObj.status isEqualToString:@"called"]];
            CallCell.datetime.text = [callObj getTimestampString];
            cell = CallCell;
        }
            return cell;
            break;
    }
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath section]) {
        case 0:
            return 200;
            break;
        case 1:
        case 2:
        case 3:
            return 80;
            break;
            
        default:
            return 0;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 3){
        return 55;
    }else{
        return 0;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2){
        return 0;
    }else{
        return 20;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 3){
        static NSString *CellIdentifier = @"TransparentHeaderCell";
        TransparentHeader *TransparentHeaderCell = (TransparentHeader *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        TransparentHeaderCell.backgroundColor = [UIColor whiteColor];
        
        if (TransparentHeaderCell == nil) {
            TransparentHeaderCell = [[TransparentHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        TransparentHeaderCell.title.text = @"通話紀錄";
        return TransparentHeaderCell;
    }else{
        return nil;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    switch (section) {
        case 3:
        {
            CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
            UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
            UIView *backgroundview = [[UIView alloc] initWithFrame:Rect];
            backgroundview.backgroundColor = [UIColor whiteColor];
            [cell addSubview:backgroundview];
            cell.backgroundColor = [UIColor colorWithRed:88/255.0f green:125/255.0f blue:139/255.0f alpha:1];
            
            UIBezierPath *maskPath = [UIBezierPath
                                      bezierPathWithRoundedRect:backgroundview.bounds
                                      byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                      cornerRadii:CGSizeMake(15, 15) ];
            CAShapeLayer *maskLayer = [CAShapeLayer layer];
            
            maskLayer.frame = backgroundview.bounds;
            maskLayer.path = maskPath.CGPath;
            backgroundview.layer.mask = maskLayer;
            return cell;
        }
            break;
        case 2:
            return nil;
            break;
        default:
        {
            CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
            UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
            break;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch ([indexPath section]) {
        case 1:
        {
            [self goTakeQRCode];
        }
            break;
        case 2:
        {
            [self goPackageView];
        }
            break;
        default:
            break;
    }
    
}
#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self.tabBarController presentViewController:NotifyView animated:YES completion:nil];
}

-(void)goShareQRCode{
    ShowQRCodeViewController *ShowQRCodeView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ShowQRCodeView"];
    ShowQRCodeView.qrcodeType = QRCodeTypeShare;
    User *user = [delegate.me getUser];
    ShowQRCodeView.qrcodeStr = user.shareQRCODE;
    [self.tabBarController presentViewController:ShowQRCodeView animated:YES completion:nil];
}
-(void)goTakeQRCode{
    ShowQRCodeViewController *ShowQRCodeView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ShowQRCodeView"];
    ShowQRCodeView.qrcodeType = QRCodeTypeTake;
    User *user = [delegate.me getUser];
    ShowQRCodeView.qrcodeStr = user.takeQRCODE;
    [self.tabBarController presentViewController:ShowQRCodeView animated:YES completion:nil];
}
-(void)goAccountView{
    ResidentViewController *ResidentView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ResidentView"];
    [self.navigationController pushViewController:ResidentView animated:YES];
}
-(void)goPackageView{
    [self.tabBarController setSelectedIndex:0];
}
-(IBAction)goClose:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
