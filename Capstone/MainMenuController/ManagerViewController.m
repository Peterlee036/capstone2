//
//  ManagerViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ManagerViewController.h"
#import "../TableViewCell/SectionHeaderTableViewCell.h"
#import "../TableViewCell/ProfileTableViewCell.h"
#import "../TableViewCell/QRcodeTableViewCell.h"
#import "../TableViewCell/TextfieldTableViewCell.h"
#import "NotifyViewController.h"
@interface ManagerViewController (){
    AppDelegate *delegate;
}

@end
@implementation ManagerViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    ManagerRepo = [[ManagerRepository alloc] init];
    ManagerRepo.Realm = delegate.DefaultRealm;
    [ManagerRepo getManagerFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self initDatasource];
    }];
    
}
-(void)viewWillAppear:(BOOL)animated{
    if (ManagerRepo.manager){
        [self initDatasource];
    }
    
    [self initNotifyBadge];
}
-(void)initDatasource{
    [loadingView stopAnimating];
    datasource = @[@{@"title":@"姓名",@"value":ManagerRepo.manager.name},
                   @{@"title":@"手機",@"value":ManagerRepo.manager.cellphone},
                   @{@"title":@"聯絡地址",@"value":ManagerRepo.manager.address}];
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (ManagerRepo.manager){
        switch (section) {
            case 0:
                return 1;
                break;
            case 1:
                return [datasource count];
            default:
                return 0;
                break;
        }
    }else{
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell;
    NSString *CellIdentifier;
    switch ([indexPath section]) {
        case 0:
        {
            CellIdentifier = @"ProfileCell";
            ProfileTableViewCell *ProfileCell = (ProfileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (ProfileCell == nil) {
                ProfileCell = [[ProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            ProfileCell.address.text = ManagerRepo.manager.customer.address;
            ProfileCell.address_detail.text = @"";
            ProfileCell.name.text = ManagerRepo.manager.customer.name;
            ProfileCell.note.text = ManagerRepo.manager.customer.cellphone;
            
            cell = ProfileCell;
        }
            return cell;
            break;
        case 1:
        default:
        {
            CellIdentifier = @"TextFieldCell";
            TextfieldTableViewCell *TextFieldCell = (TextfieldTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (TextFieldCell == nil) {
                TextFieldCell = [[TextfieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            NSDictionary *rowData = [datasource objectAtIndex:[indexPath row]];
            TextFieldCell.title.text = [rowData objectForKey:@"title"];
            TextFieldCell.value.text = [rowData objectForKey:@"value"];
            cell = TextFieldCell;
        }
            
            return cell;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath section]) {
        case 0:
            return 200;
            break;
        case 1:
            return 80;
        default:
            return 0;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return 55;
    }else{
        return 0;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (ManagerRepo.manager){
        switch (section) {
            case 1:
            {
                static NSString *CellIdentifier = @"SectionHeader";
                SectionHeaderTableViewCell *SectionCell = (SectionHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (SectionCell == nil) {
                    SectionCell = [[SectionHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                SectionCell.title.text = @"管理員・聯絡資訊";
                return SectionCell;
            }
                break;
            default:
            {
                return nil;
            }
                break;
        }
    }else{
        return nil;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    switch (section) {
        case 1:
        {
            CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
            UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
            UIView *backgroundview = [[UIView alloc] initWithFrame:Rect];
            backgroundview.backgroundColor = [UIColor whiteColor];
            [cell addSubview:backgroundview];
            cell.backgroundColor = [UIColor colorWithRed:88/255.0f green:125/255.0f blue:139/255.0f alpha:1];
            
            UIBezierPath *maskPath = [UIBezierPath
                                      bezierPathWithRoundedRect:backgroundview.bounds
                                      byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                      cornerRadii:CGSizeMake(15, 15) ];
            CAShapeLayer *maskLayer = [CAShapeLayer layer];
            
            maskLayer.frame = backgroundview.bounds;
            maskLayer.path = maskPath.CGPath;
            backgroundview.layer.mask = maskLayer;
            return cell;
        }
            break;
        default:
        {
            CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
            UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
            break;
    }
}
#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self.tabBarController presentViewController:NotifyView animated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
