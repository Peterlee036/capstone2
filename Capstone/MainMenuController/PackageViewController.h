//
//  PackageViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
#import "../Repository/PackageRepository.h"

@interface PackageViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    IBOutlet UITableView *mainTableview;
    PackageRepository *PackageRepo;
     IBOutlet UIButton *nBadge;
}

@end 
