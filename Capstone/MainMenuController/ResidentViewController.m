//
//  ResidentViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ResidentViewController.h"
#import "SCLAlertView.h"
#import "../TableViewCell/TransparentHeader.h"
#import "../TableViewCell/ResidentTableViewCell.h"
#import "NotifyViewController.h"
@interface ResidentViewController ()

@end

@implementation ResidentViewController{
    AppDelegate *delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    ResidentRepo = [[ResidentRepository alloc] init];
    ResidentRepo.Realm = delegate.DefaultRealm;
    [ResidentRepo getResidentFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
        [self->mainTableview reloadData];
        [self->mainTableview setContentOffset:CGPointZero animated:YES];
    }];
    mainTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)viewWillAppear:(BOOL)animated{
    [mainTableview reloadData];
    [mainTableview setContentOffset:CGPointZero animated:YES];
    
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger numberOfrow = ResidentRepo.Result.count;
    if (numberOfrow == 0){
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"查詢結果沒有資料";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
    }else{
        tableView.backgroundView = nil;
    }
    return numberOfrow;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ResidentCell";
    ResidentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ResidentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Resident *_resident = [ResidentRepo.Result objectAtIndex:[indexPath row]];
    cell.title.text = _resident.name;
    cell.value.text = _resident.email;
    cell.deleteBtn.tag = [indexPath row];
    [cell.deleteBtn addTarget:self action:@selector(deleteResideBtn:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *CellIdentifier = @"TransparentHeaderCell";
    TransparentHeader *TransparentHeaderCell = (TransparentHeader *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    TransparentHeaderCell.backgroundColor = [UIColor whiteColor];
    
    if (TransparentHeaderCell == nil) {
        TransparentHeaderCell = [[TransparentHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    TransparentHeaderCell.title.text = @"管理住戶帳號";
    return TransparentHeaderCell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
    UIView *backgroundview = [[UIView alloc] initWithFrame:Rect];
    backgroundview.backgroundColor = [UIColor whiteColor];
    [cell addSubview:backgroundview];
    cell.backgroundColor = [UIColor colorWithRed:88/255.0f green:125/255.0f blue:139/255.0f alpha:1];
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:backgroundview.bounds
                              byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(15, 15) ];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = backgroundview.bounds;
    maskLayer.path = maskPath.CGPath;
    backgroundview.layer.mask = maskLayer;
    return cell;
}
-(void)deleteResideBtn:(UIButton*)sender{
    Resident *_resident = [ResidentRepo.Result objectAtIndex:[sender tag]];
    UIColor *tinColor = [UIColor colorWithRed:253/255.0f green:137/255.0f blue:45/255.0f alpha:1.0];
    
    SCLAlertView *SCLAlert = [[SCLAlertView alloc] initWithNewWindowWidth:self.view.frame.size.width*0.8];
    SCLAlert.backgroundType = SCLAlertViewBackgroundBlur;
    SCLAlert.customViewColor = tinColor;
    SCLAlert.shouldDismissOnTapOutside = YES;
    
    SCLButton *SubmitBtn = [SCLAlert addButton:@"確定刪除" actionBlock:^{
        [self->ResidentRepo deleteResident:_resident completion:^(BOOL Successful, long StatusCode, NSString *Message) {
            [self->mainTableview reloadData];
        }];
    }];
    SubmitBtn.buttonFormatBlock = ^NSDictionary *{
        NSMutableDictionary *btnConfig = [[NSMutableDictionary alloc] init];
        btnConfig[@"backgroundColor"] = [UIColor colorWithRed:0/255.0f green:147/255.0f blue:229/255.0f alpha:1.0];
        btnConfig[@"textColor"] =[UIColor whiteColor];
        return  btnConfig;
    };
    
    [SCLAlert showInfo:[NSString stringWithFormat:@"確定要刪除 %@ 帳戶資料?",_resident.name] subTitle:@"按下確定刪除鍵後，該帳戶資料將完全刪除，若需重新啟用，需重新綁定。" closeButtonTitle:nil duration:0.f];
    
}
#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self presentViewController:NotifyView animated:YES completion:nil];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
