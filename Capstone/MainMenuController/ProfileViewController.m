//
//  ProfileViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ProfileViewController.h"
#import "../TableViewCell/SectionHeaderTableViewCell.h"
#import "../TableViewCell/ProfileTableViewCell.h"
#import "../TableViewCell/QRcodeTableViewCell.h"
#import "../TableViewCell/TextfieldTableViewCell.h"
#import "ShowQRCodeViewController.h"
#import "ResidentViewController.h"
#import "ModifyUserViewController.h"
#import "NotifyViewController.h"
@interface ProfileViewController (){
    AppDelegate *delegate;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate = self;
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     [mainTableview setContentOffset:CGPointZero animated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    user = [delegate.me getUser];
    NSLog(@"profile viewWillAppear user:%@",user);
    datasource = @[@{@"title":@"姓名",@"value":user.name},
                   @{@"title":@"手機",@"value":user.cellphone},
                   @{@"title":@"Email",@"value":user.email}];
    [mainTableview reloadData];
    [self initNotifyBadge];
}
-(void)initNotifyBadge{
    if ([delegate.notifyBadge intValue] > 0){
        [nBadge setSelected:YES];
    }else{
        [nBadge setSelected:NO];
    }
}
#pragma mark UINavigation Delegate
- (void)navigationController:(UINavigationController *)navigationController
        willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
      [self viewWillAppear:animated];
  }
#pragma mark TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
            return 1;
            break;
        case 2:
            return [datasource count];
        default:
            return 0;
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell;
    NSString *CellIdentifier;
    switch ([indexPath section]) {
        case 0:
        {
            CellIdentifier = @"ProfileCell";
            ProfileTableViewCell *ProfileCell = (ProfileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (ProfileCell == nil) {
                ProfileCell = [[ProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            [ProfileCell.func1Btn addTarget:self action:@selector(goShareQRCode) forControlEvents:UIControlEventTouchUpInside];
            [ProfileCell.func2Btn addTarget:self action:@selector(goAccountView) forControlEvents:UIControlEventTouchUpInside];
            
            ProfileCell.name.text = user.name;
            ProfileCell.address.text = user.placeName;
            ProfileCell.address_detail.text = user.placeAddress;
            ProfileCell.note.text = user.remarks;
            cell = ProfileCell;
        }
            return cell;
            break;
        case 1:
        {
            CellIdentifier = @"QRCodeCell";
            QRcodeTableViewCell *QRCodeCell = (QRcodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (QRCodeCell == nil) {
                QRCodeCell = [[QRcodeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            UIImage *qrcode = [Tools GeneratorQRCodeFromString:user.takeQRCODE size:150.0f];
            [QRCodeCell.QRcodeImage setImage:qrcode];
            cell = QRCodeCell;
        }
            return cell;
            break;
        case 2:
        default:
        {
            CellIdentifier = @"TextFieldCell";
            TextfieldTableViewCell *TextFieldCell = (TextfieldTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (TextFieldCell == nil) {
                TextFieldCell = [[TextfieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            NSDictionary *rowData = [datasource objectAtIndex:[indexPath row]];
            TextFieldCell.title.text = [rowData objectForKey:@"title"];
            TextFieldCell.value.text = [rowData objectForKey:@"value"];
            cell = TextFieldCell;
        }
            return cell;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath section]) {
        case 0:
            return 200;
            break;
        case 1:
        case 2:
            return 80;
            break;
        default:
            return 0;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2){
        return 55;
    }else{
        return 0;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 2:
        {
            static NSString *CellIdentifier = @"SectionHeader";
            SectionHeaderTableViewCell *SectionCell = (SectionHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (SectionCell == nil) {
                SectionCell = [[SectionHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            SectionCell.title.text = @"個人資訊";
            [SectionCell.editBtn addTarget:self action:@selector(goEditProfile) forControlEvents:UIControlEventTouchUpInside];
            return SectionCell;
        }
            break;
        default:
        {
            return nil;
        }
            break;
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    switch (section) {
        case 2:
        {
            CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
            UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
            UIView *backgroundview = [[UIView alloc] initWithFrame:Rect];
            backgroundview.backgroundColor = [UIColor whiteColor];
            [cell addSubview:backgroundview];
            cell.backgroundColor = [UIColor colorWithRed:88/255.0f green:125/255.0f blue:139/255.0f alpha:1];
            
            UIBezierPath *maskPath = [UIBezierPath
                                      bezierPathWithRoundedRect:backgroundview.bounds
                                      byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                      cornerRadii:CGSizeMake(15, 15) ];
            CAShapeLayer *maskLayer = [CAShapeLayer layer];
            
            maskLayer.frame = backgroundview.bounds;
            maskLayer.path = maskPath.CGPath;
            backgroundview.layer.mask = maskLayer;
            return cell;
        }
            break;
        default:
        {
            CGRect Rect = CGRectMake(0, 0, tableView.frame.size.width, 20);
            UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:Rect];
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
            break;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath section] == 1){
        [self goTakeQRCode];
    }
}
-(void)goTakeQRCode{
    ShowQRCodeViewController *ShowQRCodeView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ShowQRCodeView"];
    ShowQRCodeView.qrcodeType = QRCodeTypeTake;
    User *user = [delegate.me getUser];
    ShowQRCodeView.qrcodeStr = user.takeQRCODE;
    [self.tabBarController presentViewController:ShowQRCodeView animated:YES completion:nil];
}

#pragma mark IBAction Method
-(IBAction)goNotifyView:(id)sender{
    NotifyViewController *NotifyView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"NotifyView"];
    [self.tabBarController presentViewController:NotifyView animated:YES completion:nil];
}
-(void)goShareQRCode{
    ShowQRCodeViewController *ShowQRCodeView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ShowQRCodeView"];
    ShowQRCodeView.qrcodeType = QRCodeTypeShare;
    User *user = [delegate.me getUser];
    ShowQRCodeView.qrcodeStr = user.shareQRCODE;
    [self.tabBarController presentViewController:ShowQRCodeView animated:YES completion:nil];
}
-(void)goAccountView{
    ResidentViewController *ResidentView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ResidentView"];
    [self.navigationController pushViewController:ResidentView animated:YES];
}
-(void)goEditProfile{
    ModifyUserViewController *ModifyUserView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"ModifyUserView"];
    [self.navigationController pushViewController:ModifyUserView animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
