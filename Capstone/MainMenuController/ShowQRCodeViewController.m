//
//  ShowQRCodeViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ShowQRCodeViewController.h"

@interface ShowQRCodeViewController ()

@end

@implementation ShowQRCodeViewController
@synthesize qrcodeStr,qrcodeType;
- (void)viewDidLoad {
    [super viewDidLoad];
    bgview.layer.cornerRadius = 15.0f;
    headerArray = @[@"認證QRCode",@"分享註冊QR Code",@"您的專屬取貨QR Code"];
     describeArray = @[@"這是認證QRCode，我也不知道什麼時候會顯示",@"注意！此QR Code僅供住戶代表分享給其戶號下其他房客註冊APP使用。",@"請將此QRCode 貼近機器"];
}
-(void)viewWillAppear:(BOOL)animated{
    headerLabel.text = [headerArray objectAtIndex:self.qrcodeType];
    describe.text = [describeArray objectAtIndex:self.qrcodeType];
   UIImage *qrcodeImg = [Tools GeneratorQRCodeFromString:self.qrcodeStr size:qrcodeImageView.frame.size.width];
    [qrcodeImageView setImage:qrcodeImg];
    
}
#pragma mark IBAction Method
-(IBAction)goClose:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
