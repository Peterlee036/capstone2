//
//  CallTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *name;
@property (nonatomic,weak) IBOutlet UILabel *datetime;
@property (nonatomic,weak) IBOutlet UILabel *status;
- (void)setStatusCalled:(BOOL)isCalled;
@end

NS_ASSUME_NONNULL_END
