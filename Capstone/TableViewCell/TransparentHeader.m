//
//  TransparentTableViewCell.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "TransparentHeader.h"

@implementation TransparentHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)drawRect:(CGRect)rect{
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                    byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight
                    cornerRadii: (CGSize){15, 15}].CGPath;
    _bgView.layer.mask = maskLayer;
}
- (void)addSubview:(UIView *)view{
    //如果是_UITableViewCellSeparatorView就不让添加
    if (![view isKindOfClass:[NSClassFromString(@"_UITableViewCellSeparatorView") class]] && view) {
        [super addSubview:view];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
