//
//  NotifyTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotifyTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UILabel *datetime;
@property (nonatomic,weak) IBOutlet UILabel *desc;
@property (nonatomic,weak) IBOutlet UIView *headerBar;
@end

NS_ASSUME_NONNULL_END
