//
//  CallTableViewCell.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "CallTableViewCell.h"

@implementation CallTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setStatusCalled:(BOOL)isCalled{
    if (isCalled){
        _status.text = @"已接通";
        _status.textColor = [UIColor colorWithRed:0/255.0f green:147/255.0f blue:229/255.0f alpha:1];
    }else{
         _status.text = @"未接通";
        _status.textColor = [UIColor colorWithRed:235/255.0f green:96/255.0f blue:96/255.0f alpha:1];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
