//
//  TextfieldTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/11.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextfieldTableViewCell : UITableViewCell


@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UITextField *textfield;
@property (nonatomic,weak) IBOutlet UILabel *value;
@end

NS_ASSUME_NONNULL_END
