//
//  QRcodeTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/17.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QRcodeTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *QRcodeImage;
@end

NS_ASSUME_NONNULL_END
