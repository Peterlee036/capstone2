//
//  PackageInfoTableViewCell.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "PackageInfoTableViewCell.h"

@implementation PackageInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
     _bgview.layer.cornerRadius = 15.0f;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
