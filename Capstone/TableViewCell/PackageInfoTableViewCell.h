//
//  PackageInfoTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PackageInfoTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UILabel *put_at;
@property (nonatomic,weak) IBOutlet UILabel *duration;
@property (nonatomic,weak) IBOutlet UILabel *serial;
@property (nonatomic,weak) IBOutlet UIView *bgview;
@end

NS_ASSUME_NONNULL_END
