//
//  TransparentTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/18.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransparentHeader : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UIView *bgView;
@end

NS_ASSUME_NONNULL_END
