//
//  ProfileTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/17.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *name;
@property (nonatomic,weak) IBOutlet UILabel *note;
@property (nonatomic,weak) IBOutlet UILabel *address;
@property (nonatomic,weak) IBOutlet UILabel *address_detail;
@property (nonatomic,weak) IBOutlet UIButton *func1Btn;
@property (nonatomic,weak) IBOutlet UIButton *func2Btn;

@end

NS_ASSUME_NONNULL_END
