//
//  ProfileTableViewCell.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/17.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = 15.0f;
    self.layer.cornerRadius = 15.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
