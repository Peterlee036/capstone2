//
//  ResidentTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResidentTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UIButton *deleteBtn;
@property (nonatomic,weak) IBOutlet UILabel *value;
@end

NS_ASSUME_NONNULL_END
