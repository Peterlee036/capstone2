//
//  TextfieldTableViewCell.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/11.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "TextfieldTableViewCell.h"

@implementation TextfieldTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
