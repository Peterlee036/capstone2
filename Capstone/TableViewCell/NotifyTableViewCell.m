//
//  NotifyTableViewCell.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "NotifyTableViewCell.h"

@implementation NotifyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _headerBar.layer.cornerRadius = 2.5f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
