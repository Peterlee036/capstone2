//
//  PackageHeaderTableViewCell.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PackageHeaderTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *bgView1;
@property (nonatomic,weak) IBOutlet UIView *bgView2;
@property (nonatomic,weak) IBOutlet UIButton *PageageBtn1;
@property (nonatomic,weak) IBOutlet UIButton *PageageBtn2;


@end

NS_ASSUME_NONNULL_END
