//
//  Tools.h
//  HS_Member
//
//  Created by Peter Lee on 2018/10/19.
//  Copyright © 2018 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define IS_IPAD_PRO (IS_IPAD && SCREEN_MAX_LENGTH >= 1366.0)


NS_ASSUME_NONNULL_BEGIN

@interface Tools : NSObject

typedef NS_ENUM(NSUInteger, deviceType) {
    deviceTypeDefault = 0,
    deviceTypeiPad,
    deviceTypeiPadPro,
    deviceTypeiPhone4s,
    deviceTypeiPhone5,
    deviceTypeiPhone6,
    deviceTypeiPhonePlus,
    deviceTypeiPhoneX
};


+(float)Get_StatusBar_Height;
+(BOOL)is_iphoneX_screen;
+(NSString *)NSStringtoMD5:(NSString *)input;
+(BOOL)isEmailFormat:(NSString*)email;
+(BOOL)ifPhoneNumberCharacterSet:(NSString*)Text;
+(BOOL)isIdNumberFormat:(NSString *)id_number;
+(deviceType)getCurrentDeviceType;
+(UIImage *)GeneratorQRCodeFromString:(NSString *)code size:(CGFloat)size;
+(UIImage *)createfNonInterpolatedImageFromCIImage:(CIImage *)iamge withSize:(CGFloat)size;
@end

NS_ASSUME_NONNULL_END
