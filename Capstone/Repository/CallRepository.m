//
//  CallRepository.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "CallRepository.h"
@implementation CallRepository
- (instancetype) init{
    if (self = [super init]) {
        connectObj = [[ConnectObj alloc] init];
        self.Result = [Call allObjects];
        
        return self;
    }else{
        return nil;
    }
}
-(void)getCallFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    [connectObj connectAPIWithType:@"getCallLog" Parameter:nil Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
         if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            NSArray *RawData = [Result objectForKey:@"data"];
            for (NSDictionary *Data in RawData){
                Call *_call = [[Call alloc] init];
                [_call getCallReady:Data];
                [self.Realm transactionWithBlock:^{
                    [self.Realm addOrUpdateObject:_call];
                }];
            }
            if (completion){
                completion(YES,StatusCode,@"取得通話紀錄");
            }
         }else{
             if (completion){
                 NSLog(@"%i %@ fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
                 completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
             }
         }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
@end
