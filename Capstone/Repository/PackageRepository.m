//
//  PackageRepository.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "PackageRepository.h"

@implementation PackageRepository
- (instancetype) init{
    if (self = [super init]) {
        connectObj = [[ConnectObj alloc] init];
        return self;
    }else{
        return nil;
    }
}
-(void)getPackageFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    [connectObj connectAPIWithType:@"getPackage" Parameter:nil Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
        
         if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            NSArray *RawData = [Result objectForKey:@"data"];
            for (NSDictionary *Data in RawData){
                Package *_package = [[Package alloc] init];
                [_package getPackageReady:Data];
                [self.Realm transactionWithBlock:^{
                    [self.Realm addOrUpdateObject:_package];
                }];
                [self getPackageUnTaked];
            }
            if (completion){
                completion(YES,StatusCode,@"取得包裹紀錄");
            }
         }else{
             if (completion){
                 NSLog(@"%i %@ fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
                 completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
             }
         }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
-(void)getPackageAll{
     self.Result = [Package allObjects];
}
-(void)getPackageUnTaked{
    self.Result = [Package objectsWhere:@"status == 'untake'"];
}
-(void)getPackageTaked{
     self.Result = [Package objectsWhere:@"status != 'untake'"];
}

@end
