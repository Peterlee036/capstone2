//
//  ManagerRepository.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "../Model/ConnectObj.h"
#import "../Model/Manager.h"

@interface ManagerRepository : NSObject{
    ConnectObj *connectObj;
}
@property RLMRealm *Realm;
@property RLMResults *Result;
@property Manager *manager;
-(void)getManagerFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion;
@end

