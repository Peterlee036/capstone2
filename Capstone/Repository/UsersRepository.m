//
//  UsersRepository.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/8.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "UsersRepository.h"
#import "../Tools.h"
@implementation UsersRepository
@synthesize Realm,Result,LoginStatus,signupUser;
- (instancetype) init{
    if (self = [super init]) {
        connectObj = [[ConnectObj alloc] init];
        return self;
    }else{
        return nil;
    }
}
-(User*)getUser{
    if (!Result){
        Result = [User allObjects];
        if (Result.count > 0){
            self.LoginStatus = UserLoginStatusLogin;
            return [Result firstObject];
        }else{
            self.LoginStatus = UserLoginStatusSignup;
            return nil;
        }
    }else{
        return [Result firstObject];
    }
}
-(void)login{
    self.signupUser = [self getUser];
    if (!self.signupUser){
        self.signupUser = [[User alloc] init];
//        假資料
        NSDictionary *parameter =  @{
              @"qrcode": @"BB5C7C672206AF728624B5E81EF285B9",
              @"name": @"奈普住戶代表",
              @"cellphone": @"0935760032",
              @"email": @"peter@ne-plus.com",
              @"takePassword": @"12345",
              @"firebaseToken": @"dsadgrgfgfdsfdsfds",
              @"token": [self getKey]
        };
        [self Signup:parameter completion:nil];
        
    //
    }else{
        [self getUserFromAPI:^(BOOL Successful, long StatusCode, NSString *Message) {
            if (!Successful){
                 self.LoginStatus = UserLoginStatusSignup;
                NSLog(@"login Message %@",Message);
            }
        }];
    }
}

-(void)updateUserFirebaseToken:(NSString*)token{
    if (self.LoginStatus != UserLoginStatusSignup){
         User *me = [self getUser];
        [self->Realm beginWriteTransaction];
        me.firebaseToken = token;
        [self->Realm commitWriteTransaction];
        [self updateUserFromAPI:nil completion:nil];
        NSLog(@"updateUserFirebaseToken token %@",token);
    }
}
-(void)updateUserAccessToken:(NSString*)token{
    User *user = [self getUser];
    if (user){
        [self->Realm beginWriteTransaction];
               user.accessToken = token;
           [self->Realm commitWriteTransaction];
    }else{
        self->signupUser.accessToken = token;
    }
   
}
#pragma mark - Signup method
-(NSString*)getKey{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Taipei"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    return  [[Tools NSStringtoMD5:[dateFormatter stringFromDate:[NSDate date]]] uppercaseString];
}
-(void)Signup:(NSDictionary *)parameter completion:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    if (!parameter){
        parameter =  @{@"qrcode":self.signupUser.certificationQRCODE,
                       @"name":self.signupUser.name,
                       @"email":self.signupUser.email,
                       @"cellphone":self.signupUser.cellphone,
                       @"takePassword":self.signupUser.takePassword,
                       @"firebaseToken":@"",
                       @"token":[self getKey]};
    }
    [connectObj connectAPIWithType:@"bindAccount" Parameter:parameter Method:@"POST" useToken:NO Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
         if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            if ([Result objectForKey:@"accessToken"]){
                self.signupUser.accessToken = [Result objectForKey:@"accessToken"];
            }
            [self getUserFromAPI:nil];
            if (completion){
                completion(YES,StatusCode,@"註冊成功！");
            }
        }else{
            if (completion){
                  completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
            }
        }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
             completion(NO,StatusCode,Message);
        }
    }];
}

#pragma mark - API Sync method
-(void)getUserFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    [connectObj connectAPIWithType:@"getResidentInformation" Parameter:nil Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
         if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            NSArray *RawData = [Result objectForKey:@"data"];
            if ([RawData count] != 0){
                User *user = [self getUser];
                if (user){
                    //如果user = true 的代表之前已經登入過
                    [self->Realm beginWriteTransaction];
                    [user getUserReady:[RawData firstObject]];
                    [self->Realm commitWriteTransaction];
                }else{
                    //如果user = false 的代表第一次登入
                   if (!self.signupUser.ID){
                        self.signupUser.ID = [[RawData firstObject] objectForKey:@"id"];
                   }
                    
                    [self.signupUser getUserReady:[RawData firstObject]];
                    [self.Realm transactionWithBlock:^{
                        [self.Realm addOrUpdateObject:self.signupUser];
                    }];
                    self.signupUser = [self getUser]; //這裡會更新user的登入狀態
                }
              
            }
            if (completion){
                completion(YES,StatusCode,@"取得User資料");
            }
         }else{
             NSLog(@"%i %@ api fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
             if (completion){
                 completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
             }
         }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        NSLog(@"%li %@ notwork fails! result %@",StatusCode,Message,Result);
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
-(void)updateUserFromAPI:(NSMutableDictionary *)parameter completion:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    User *user = [self getUser];
    if (!parameter){
        parameter = [[NSMutableDictionary alloc] init];
    }
    if (user){
         [parameter setValue:[NSString stringWithFormat:@"%@",user.firebaseToken] forKey:@"firebaseToken"];
    }
    [parameter setValue:[self getKey] forKey:@"token"];
   
    [connectObj connectAPIWithType:@"updateResident" Parameter:parameter Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
        if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            if ([[Result objectForKey:@"result"] isEqualToString:@"true"]){
                [self->Realm beginWriteTransaction];
                [user updateUser:parameter];
                [self->Realm commitWriteTransaction];
            }
            if (completion){
                completion(YES,StatusCode,@"更新User資料");
            }
        }else{
            NSLog(@"%i %@ api fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
            if (completion){
                completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
            }
        }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        NSLog(@"%li %@ notwork fails! result %@",StatusCode,Message,Result);
        if (completion){
              completion(NO,StatusCode,Message);
        }
    }];
}
@end
