//
//  NotifyRepository.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Realm/Realm.h>
#import "../Model/ConnectObj.h"
#import "../Model/Notify.h"

@interface NotifyRepository : NSObject{
    ConnectObj *connectObj;
}
@property RLMRealm *Realm;
@property RLMResults *Result;
-(void)getNotiyFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion;

@end
