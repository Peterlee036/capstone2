//
//  PackageRepository.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "../Model/ConnectObj.h"
#import "../Model/Package.h"

@interface PackageRepository : NSObject{
    ConnectObj *connectObj;
}
@property RLMRealm *Realm;
@property RLMResults<Package *> *Result;
-(void)getPackageFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion;
-(void)getPackageAll;
-(void)getPackageUnTaked;
-(void)getPackageTaked;
@end

