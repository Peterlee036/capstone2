//
//  NotifyRepository.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/24.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "NotifyRepository.h"
@implementation NotifyRepository
- (instancetype) init{
    if (self = [super init]) {
        connectObj = [[ConnectObj alloc] init];
        self.Result = [Notify allObjects];
        return self;
    }else{
        return nil;
    }
}
-(void)getNotiyFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    [connectObj connectAPIWithType:@"getNotice" Parameter:nil Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
         if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            NSArray *RawData = [Result objectForKey:@"data"];
            for (NSDictionary *Data in RawData){
                Notify *_notify = [[Notify alloc] init];
                [_notify getNotifyReady:Data];
                [self.Realm transactionWithBlock:^{
                    [self.Realm addOrUpdateObject:_notify];
                }];
            }
            if (completion){
                completion(YES,StatusCode,@"取得推播紀錄");
            }
        }else{
            if (completion){
                NSLog(@"%i %@ fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
                completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
            }
        }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
@end
