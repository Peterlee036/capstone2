//
//  UsersRepository.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/8.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "../Model/ConnectObj.h"
#import "../Model/User.h"

typedef NS_ENUM(NSUInteger, UserLoginStatus) {
    UserLoginStatusSignup = 0,
    UserLoginStatusLogin
};

@interface UsersRepository : NSObject{
     ConnectObj *connectObj;
}

@property RLMRealm *Realm;
@property RLMResults *Result;
@property User *signupUser;
@property UserLoginStatus LoginStatus;
-(User*)getUser;
-(void)login;
-(void)Signup:(NSDictionary *)parameter completion:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion;
-(void)getUserFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion;
-(void)updateUserFromAPI:(NSMutableDictionary *)parameter completion:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion;

-(void)updateUserAccessToken:(NSString*)token;
-(void)updateUserFirebaseToken:(NSString*)token;


@end


