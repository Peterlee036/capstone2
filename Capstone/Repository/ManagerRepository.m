//
//  ManagerRepository.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ManagerRepository.h"

@implementation ManagerRepository
@synthesize Realm,Result,manager;
- (instancetype) init{
    if (self = [super init]) {
        connectObj = [[ConnectObj alloc] init];
        self.Result = [Manager allObjects];
        [self getManagerFromRealm];
       
        return self;
    }else{
        return nil;
    }
}
-(void)getManagerFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    [connectObj connectAPIWithType:@"getPlaceManager" Parameter:nil Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
         if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            NSArray *RawData = [Result objectForKey:@"data"];
            if ([RawData count] > 0){
                NSArray *ManagerData =  [[RawData firstObject] objectForKey:@"manager"];
                self.manager = [[Manager alloc] init];
                for (NSDictionary *mData in ManagerData){
                    [self.manager getManagerReady:mData];
                }
                NSArray *CostomerData =  [[RawData firstObject] objectForKey:@"customer"];
                self.manager.customer = [[Customer alloc] init];
                for (NSDictionary *cData in CostomerData){
                    [self.manager.customer getCustomerReady:cData];
                }
            }
            if (self.manager){
                [self.Realm transactionWithBlock:^{
                    [self.Realm addOrUpdateObject:self.manager];
                }];
            }
            [self getManagerFromRealm];
            if (completion){
                completion(YES,StatusCode,@"取得Manager資料");
            }
         }else{
             if (completion){
                 NSLog(@"%i %@ fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
                 completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
             }
         }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
-(void)getManagerFromRealm{
    if ([self.Result count] > 0){
        self.manager = [self.Result firstObject];
    }
}
@end
