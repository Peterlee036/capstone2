//
//  ResidentRepository.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/19.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "ResidentRepository.h"
@implementation ResidentRepository
- (instancetype) init{
    if (self = [super init]) {
        connectObj = [[ConnectObj alloc] init];
        self.Result = [Resident allObjects];
        
        return self;
    }else{
        return nil;
    }
}
-(void)getResidentFromAPI:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    [connectObj connectAPIWithType:@"getResident" Parameter:nil Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
        if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            NSArray *RawData = [Result objectForKey:@"data"];
            for (NSDictionary *Data in RawData){
                Resident *_resident = [[Resident alloc] init];
                [_resident getResidentReady:Data];
                [self.Realm transactionWithBlock:^{
                    [self.Realm addOrUpdateObject:_resident];
                }];
            }
            if (completion){
                completion(YES,StatusCode,@"取得住戶資料");
            }
        }else{
            if (completion){
                NSLog(@"%li %@ fails! result %@",StatusCode,Type,Result);
                completion(NO,StatusCode,@"取得住戶資料失敗！");
            }
        }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
-(void)deleteResident:(Resident*)resident completion:(void (^)(BOOL Successful,long StatusCode,NSString *Message))completion{
    NSDictionary *parameter = @{@"deleteID":resident.ID};
    [connectObj connectAPIWithType:@"deleteResident" Parameter:parameter Method:@"POST" useToken:YES Success:^(long StatusCode, NSString * _Nonnull Type, NSDictionary * _Nonnull Result) {
        if ([[Result objectForKey:@"errorcode"] intValue] == 200){
            [self.Realm beginWriteTransaction];
            [self.Realm deleteObject:resident];
            [self.Realm commitWriteTransaction];
            
            if (completion){
                completion(YES,StatusCode,@"成功刪除住戶資料");
            }
        }else{
            if (completion){
                NSLog(@"%i %@ fails! result %@",[[Result objectForKey:@"errorcode"] intValue],Type,Result);
                completion(NO,[[Result objectForKey:@"errorcode"] intValue],[Result objectForKey:@"msg"]);
            }
        }
        
    } Fail:^(long StatusCode, NSString * _Nonnull Message, NSDictionary * _Nonnull Result) {
        if (completion){
            completion(NO,StatusCode,Message);
        }
    }];
}
@end
