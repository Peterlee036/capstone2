//
//  NotSignupViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/11.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
NS_ASSUME_NONNULL_BEGIN

@interface NotSignupViewController : UIViewController{
    IBOutlet UIView *mainView;
}

@end

NS_ASSUME_NONNULL_END
