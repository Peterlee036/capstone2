//
//  QRCodeScannerViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/7.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
@import AVFoundation;

@interface QRCodeScannerViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate>{
    IBOutlet NSLayoutConstraint *NavHeight;
    AVCaptureSession *captureSession;
    AVCaptureDevice *captureDevice;
    AVCaptureVideoPreviewLayer *videoPreviewLayer;
    IBOutlet UIView *cameraView;
}
@end

