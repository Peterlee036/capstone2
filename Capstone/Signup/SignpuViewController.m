//
//  SignpuViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/11.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "SignpuViewController.h"
#import "SCLAlertView.h"
#import "../TableViewCell/TextfieldTableViewCell.h"
#import "../TableViewCell/SectionHeaderTableViewCell.h"
@interface SignpuViewController (){
    AppDelegate *delegate;
}

@end

@implementation SignpuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    mainView.layer.cornerRadius = 15;
    submitBtn.layer.cornerRadius = 5;
    signupTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UITapGestureRecognizer *singleTaprecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(handleTapGesture:)];
    [singleTaprecognizer setNumberOfTouchesRequired : 1];
    [self.view addGestureRecognizer : singleTaprecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHiden:) name:UIKeyboardWillHideNotification object:nil];

    [self initDataSource];

}
#pragma mark - DataSource Method
-(void)initDataSource{
    SectionName = @[@"請填寫您的相關資訊，以完成帳戶綁定",@"請設定取貨的開櫃密碼"];
    NSArray *tempDataSource = @[@[@{@"title":@"姓名",@"value":@"奈普住戶代表",@"key":@"name",@"SignupFieldType":[NSNumber numberWithInt:SignupFieldTypeName]},
                     @{@"title":@"手機",@"value":@"0935760032",@"key":@"cellphone",@"SignupFieldType":[NSNumber numberWithInt:SignupFieldTypeCellPhone]},
                     @{@"title":@"E-mail",@"value":@"peter@ne-plus.com",@"key":@"email",@"SignupFieldType":[NSNumber numberWithInt:SignupFieldTypeEmail]}],
                   @[@{@"title":@"請輸入密碼",@"value":@"123456",@"key":@"takePassword",@"SignupFieldType":[NSNumber numberWithInt:SignupFieldTypePassword]},
                     @{@"title":@"再次輸入密碼",@"value":@"123456",@"key":@"takePassword",@"SignupFieldType":[NSNumber numberWithInt:SignupFieldTypePasswordConfirm]}]];
    DataSource = [tempDataSource mutableCopy];
    
    FieldIndex = [[NSMutableDictionary alloc] init];
    for (int s = 0;s<[DataSource count];s++){
        NSArray *data = [DataSource objectAtIndex:s];
        for (int i = 0;i<[data count];i++){
            NSNumber *key =  [[data objectAtIndex:i] objectForKey:@"SignupFieldType"];
            NSIndexPath *rowIndex = [NSIndexPath indexPathForRow:i inSection:s];
            [FieldIndex setObject:rowIndex forKey:key];
        }
    }
}
-(NSIndexPath*)getRowIndexByFieldType:(SignupFieldType)type{
    return  [FieldIndex objectForKey:[NSNumber numberWithInteger:type]] ;
}
-(NSDictionary *)getRowDatawithIndexPath:(NSIndexPath*)indexpath{
    NSDictionary *RowData = [[DataSource objectAtIndex:[indexpath section]] objectAtIndex:[indexpath row]];
    return RowData;
}
-(void)updateDataSourceWithFieldType:(SignupFieldType)type Value:(id)value{
    NSIndexPath *rowIndex = [self getRowIndexByFieldType:type];
    NSMutableDictionary *MutableRowData = [[self getRowDatawithIndexPath:rowIndex] mutableCopy];
    [MutableRowData setObject:value forKey:@"value"];
    NSMutableArray *SectionArray = [[DataSource objectAtIndex:[rowIndex section]] mutableCopy];
    [SectionArray replaceObjectAtIndex:[rowIndex row] withObject:MutableRowData];
    [DataSource replaceObjectAtIndex:[rowIndex section] withObject:SectionArray];
}
#pragma mark TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [DataSource count];;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[DataSource objectAtIndex:section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TextFieldCell";
    TextfieldTableViewCell *cell = (TextfieldTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TextfieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *rowData = [[DataSource objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
    int fieldType = [[rowData objectForKey:@"SignupFieldType"] intValue];
    switch (fieldType) {
        case SignupFieldTypePassword:
        case SignupFieldTypePasswordConfirm:
            cell.textfield.secureTextEntry = YES;
            break;
        default:
            cell.textfield.secureTextEntry = NO;
            break;
    }
    cell.title.text = [rowData objectForKey:@"title"];
    cell.textfield.text = [rowData objectForKey:@"value"];
    cell.textfield.placeholder = [NSString stringWithFormat:@"請輸入您的%@",[rowData objectForKey:@"title"]];
    cell.textfield.tag = fieldType;
    cell.textfield.delegate = self;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"SectionHeader";
    SectionHeaderTableViewCell *SectionCell = (SectionHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    SectionCell.backgroundColor = [UIColor whiteColor];
    
    if (SectionCell == nil) {
        SectionCell = [[SectionHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    SectionCell.title.text = [SectionName objectAtIndex:section];
    return SectionCell;
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [SectionName objectAtIndex:section];
//}
//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
//    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
//    [header setBackgroundColor:[UIColor whiteColor]];
//    [header.textLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightSemibold]];
//    header.textLabel.numberOfLines =1;
//    header.textLabel.adjustsFontSizeToFitWidth = YES;
//    [header.textLabel setTextColor:[UIColor colorWithRed:3/255.0f green:22/255.0f blue:60/255.0f alpha:1]];
//}
#pragma mark - UITextField Method
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == SignupFieldTypeCellPhone){
        //電話
        NSCharacterSet *acceptedInput = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        if ((([string rangeOfCharacterFromSet:acceptedInput].location == NSNotFound) && [textField.text length] <= 9) || [string isEqualToString:@""]){
            return  YES;
        }else{
            return NO;
        }
    }else{
        return YES;
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    firstResponse = textField;
    switch (textField.tag) {
        case SignupFieldTypePassword:
            //密碼
        case SignupFieldTypePasswordConfirm:
            //確認密碼
            textField.text = @"";
            textField.secureTextEntry = YES;
            break;

        case SignupFieldTypeCellPhone:
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.secureTextEntry = NO;
            textField.inputView = nil;
            textField.inputAccessoryView = nil;
            break;
        default:
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.secureTextEntry = NO;
            textField.inputView = nil;
            textField.inputAccessoryView = nil;
            break;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField.text length] > 0){
        switch (textField.tag) {
            case SignupFieldTypeName:
                //名稱
                if ([textField.text length] == 0){
                      [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"名稱為必填欄位"];
                    
                }else{
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }
                break;
        
            case SignupFieldTypeEmail:
                //email
                
                if ([Tools isEmailFormat:textField.text]){
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }else{
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"輸入的email不是正確格式"];
                }
                
                break;
            case SignupFieldTypeCellPhone:
                //電話
                if ([Tools ifPhoneNumberCharacterSet:textField.text]){
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }else{
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"電話號碼需為10碼數字，不包含特殊符號"];
                }
                break;
            case SignupFieldTypePassword:
                //密碼
            {
                
                NSIndexPath  *indexRow = [self getRowIndexByFieldType:SignupFieldTypePasswordConfirm];
                NSDictionary *otherRow = [self getRowDatawithIndexPath:indexRow];
                NSString *other_password = [otherRow objectForKey:@"value"];
                if ([textField.text length] < 6){
                   [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"密碼須大於或等於6碼"];
                } else if ([other_password length] != 0 && ![textField.text isEqualToString:other_password]){
                     [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"輸入的密碼不相同"];
                }else{
                    [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }
            }
                break;
            case SignupFieldTypePasswordConfirm:
                //確認密碼
            {
                NSIndexPath  *indexRow = [self getRowIndexByFieldType:SignupFieldTypePassword];
                NSDictionary *otherRow = [self getRowDatawithIndexPath:indexRow];
                NSString *other_password = [otherRow objectForKey:@"value"];
                if ([textField.text length] < 6){
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"密碼須大於或等於6碼"];
                } else if ([other_password length] != 0 && ![textField.text isEqualToString:other_password]){
                    [self ShowErrorAlertWithTitle:@"輸入錯誤" Message:@"輸入的密碼不相同"];
                }else{
                   [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
                }
            }
                break;
         
            default:
            {
                [self updateDataSourceWithFieldType:textField.tag Value:textField.text];
            }
                break;
        }
    }
}

#pragma mark - Commend Method
-(void)ShowErrorAlertWithTitle:(NSString*)title Message:(NSString*)msg {
    UIColor *tinColor = [UIColor colorWithRed:253/255.0f green:137/255.0f blue:45/255.0f alpha:1.0];
    
     SCLAlertView *SCLAlert = [[SCLAlertView alloc] initWithNewWindowWidth:self.view.frame.size.width*0.8];
    SCLAlert.backgroundType = SCLAlertViewBackgroundBlur;
    SCLAlert.customViewColor = tinColor;
    SCLAlert.shouldDismissOnTapOutside = YES;
    SCLAlert.horizontalButtons = YES;
    
//    SCLButton *CancelBtn =  [SCLAlert addButton:@"取消" actionBlock:nil];
//    CancelBtn.buttonFormatBlock = ^NSDictionary *{
//        NSMutableDictionary *btnConfig = [[NSMutableDictionary alloc] init];
//        btnConfig[@"backgroundColor"] = [UIColor whiteColor];
//        btnConfig[@"textColor"] = [UIColor lightGrayColor];
//        return btnConfig;
//    };
//
//    SCLButton *SubmitBtn = [SCLAlert addButton:@"確定執行" actionBlock:^{
//        //[self CancelTransac:transObj];
//    }];
//    SubmitBtn.buttonFormatBlock = ^NSDictionary *{
//        NSMutableDictionary *btnConfig = [[NSMutableDictionary alloc] init];
//        btnConfig[@"backgroundColor"] = tinColor
//        btnConfig[@"textColor"] =[UIColor whiteColor];
//        return  btnConfig;
//    };
    [SCLAlert showInfo:title subTitle:msg closeButtonTitle:@"再試一次" duration:0.0f];
}


-(BOOL)getVerifySignupData{
    BOOL Verified = YES;
    NSString *errorMessgae;
    for (int s = 0;s<[DataSource count];s++){
        NSMutableArray *data = [DataSource objectAtIndex:s];
        for (int i = 0;i<[data count];i++){
            NSMutableDictionary *SignupData = [data objectAtIndex:i];
            NSNumber *key =  [SignupData objectForKey:@"SignupFieldType"];
            switch ([key intValue]) {
                case SignupFieldTypeName:
                    //必填欄位
                    if ([[SignupData objectForKey:@"value"] length] == 0){
                        errorMessgae = [NSString stringWithFormat:@"%@欄位為必填",[SignupData objectForKey:@"display_name"]];
                        Verified = NO;
                    }
                    break;
                case SignupFieldTypeCellPhone:
                    if (![Tools ifPhoneNumberCharacterSet:[SignupData objectForKey:@"value"]]){
                        errorMessgae = @"電話號碼需為10碼數字，不包含特殊符號";
                        Verified = NO;
                    }
                    break;
                case SignupFieldTypeEmail:
                    if (![Tools isEmailFormat:[SignupData objectForKey:@"value"]] || [[SignupData objectForKey:@"value"] isEqualToString:@""] ){
                        errorMessgae = @"email 格式不正確";
                        Verified = NO;
                    }
                    break;
                case SignupFieldTypePassword:
                    //password
                {
                    NSIndexPath  *indexRow = [self getRowIndexByFieldType:SignupFieldTypePassword];
                    NSDictionary *otherRow = [self getRowDatawithIndexPath:indexRow];
                    if (![[SignupData objectForKey:@"value"] isEqualToString:[otherRow objectForKey:@"value"]] || [[SignupData objectForKey:@"value"] length] < 6  ){
                        Verified = NO;
                        errorMessgae = @"密碼驗證錯誤";
                    }
                    
                }
                    break;
                default:
                    break;
            }

        }
    }
    if (!Verified){
         [self ShowErrorAlertWithTitle:@"註冊資料錯誤" Message:errorMessgae];
    }
    
    return Verified;
}
#pragma mark - IBAction Delegate
-(IBAction)goSignup:(id)sender{
     [self.view endEditing:YES];
    if ([self getVerifySignupData]){
        User *me =  delegate.me.signupUser;
        me.name = [[self getRowDatawithIndexPath:[self getRowIndexByFieldType:SignupFieldTypeName]] objectForKey:@"value"];
        me.cellphone = [[self getRowDatawithIndexPath:[self getRowIndexByFieldType:SignupFieldTypeCellPhone]] objectForKey:@"value"];
        me.email = [[self getRowDatawithIndexPath:[self getRowIndexByFieldType:SignupFieldTypeEmail]] objectForKey:@"value"];
        me.takePassword = [[self getRowDatawithIndexPath:[self getRowIndexByFieldType:SignupFieldTypePassword]] objectForKey:@"value"];
        [delegate.me Signup:nil completion:^(BOOL Successful,long StatusCode,NSString *Message) {
            SCLAlertView *SCLAlert = [[SCLAlertView alloc] initWithNewWindow];
            SCLAlert.backgroundType = SCLAlertViewBackgroundBlur;
            SCLAlert.customViewColor = [UIColor colorWithRed:101/255.0f green:182/255.0f blue:88/255.0f alpha:1];
            SCLAlert.shouldDismissOnTapOutside = NO;
            //SCLAlert.horizontalButtons = YES;
                SCLButton *SubmitBtn = [SCLAlert addButton:@"開始使用" actionBlock:^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                SubmitBtn.buttonFormatBlock = ^NSDictionary *{
                    NSMutableDictionary *btnConfig = [[NSMutableDictionary alloc] init];
                    btnConfig[@"backgroundColor"] = [UIColor colorWithRed:0/255.0f green:147/255.0f blue:229/255.0f alpha:1];;
                    btnConfig[@"textColor"] =[UIColor whiteColor];
                    return  btnConfig;
                };
            [SCLAlert showSuccess:@"恭喜您綁定成功！" subTitle:@"您可以開始使用頂石雲端APP囉！" closeButtonTitle:nil duration:0.0f];
        }];

    }
   // [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Keyboard Delegate
-(void)keyboardWillShow:(NSNotification*)notification {
    NSIndexPath *row = [self getRowIndexByFieldType:firstResponse.tag];
    if ([row section] > 0){
        float newY =  200;
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.view setFrame:CGRectMake(0, 0-newY, self.view.frame.size.width, self.view.frame.size.height)];
        } completion:nil];
    }
}
-(void)keyboardWillHiden:(NSNotification*)notification {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } completion:nil];
}
-(void)handleTapGesture:(UITapGestureRecognizer *) gestureRecognizer{
    [self.view endEditing:YES];
}
-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end
