//
//  QRCodeScannerViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/7.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "QRCodeScannerViewController.h"
#import "SignpuViewController.h"
@interface QRCodeScannerViewController (){
    AppDelegate *delegate;
}

@end

@implementation QRCodeScannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NavHeight.constant = [delegate getNavigationHeight];
    [self.navigationController setNavigationBarHidden:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [self startReadQRcode];
}
-(void)startReadQRcode{
    NSError *error;
    captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (input){
        // Initialize the captureSession object.
        captureSession = [[AVCaptureSession alloc] init];
        // Set the input device on the capture session.
        [captureSession addInput:input];
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [captureSession addOutput:captureMetadataOutput];
        
        // Create a new serial dispatch queue.
        dispatch_queue_t dispatchQueue;
        dispatchQueue = dispatch_queue_create("myQueue", NULL);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
        [captureMetadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
        
        if (videoPreviewLayer){
            [videoPreviewLayer removeFromSuperlayer];
        }
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
        [videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [videoPreviewLayer setFrame:cameraView.layer.bounds];
        
        AVCaptureConnection *previewLayerConnection = videoPreviewLayer.connection;
        
        if ([previewLayerConnection isVideoOrientationSupported]){
            [previewLayerConnection setVideoOrientation:(AVCaptureVideoOrientation)[[UIApplication sharedApplication] statusBarOrientation]];
        }
        [cameraView.layer addSublayer:videoPreviewLayer];
        
        // Start video capture.
        [captureSession startRunning];
    }
}
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        @try {
            AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
            NSString *QrcodeStr = metadataObj.stringValue;
            if (QrcodeStr && [QrcodeStr length] == 32){
                NSLog(@"QrcodeStr %@",metadataObj.stringValue);
                
                [self performSelectorOnMainThread:@selector(getQrcodeAuccess:) withObject:QrcodeStr waitUntilDone:YES];
            }else{
                NSLog(@"分析錯誤");
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
}
-(void)getQrcodeAuccess:(NSString *)QRCodeString{
    [captureSession stopRunning];
    NSLog(@"QRCodeString %@",QRCodeString);
    delegate.me.signupUser.certificationQRCODE = QRCodeString;
    [self goSignup];
}
-(IBAction)goPhotoLibrary:(id)sender{
    
}
-(void)goSignup{
    SignpuViewController *SignupView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"SignupView"];
    [self.navigationController pushViewController:SignupView animated:YES];
}
-(IBAction)goClose:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
@end
