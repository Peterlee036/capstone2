//
//  SignpuViewController.h
//  Capstone
//
//  Created by Peter Lee on 2019/9/11.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../AppDelegate.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SignupFieldType) {
    SignupFieldTypeName = 0,
    SignupFieldTypeCellPhone,
    SignupFieldTypeEmail,
    SignupFieldTypePassword,
    SignupFieldTypePasswordConfirm
};
@interface SignpuViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    IBOutlet UIView *mainView;
    IBOutlet UIButton *submitBtn;
    NSArray *SectionName;
    NSMutableArray *DataSource;
    NSMutableDictionary *FieldIndex;
    IBOutlet UITableView *signupTableview;
    UITextField *firstResponse;
}

@end

NS_ASSUME_NONNULL_END
