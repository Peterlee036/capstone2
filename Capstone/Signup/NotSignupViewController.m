//
//  NotSignupViewController.m
//  Capstone
//
//  Created by Peter Lee on 2019/9/11.
//  Copyright © 2019 Peter Lee. All rights reserved.
//

#import "NotSignupViewController.h"
#import "QRCodeScannerViewController.h"
@interface NotSignupViewController (){
    AppDelegate *delegate;
}

@end

@implementation NotSignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     mainView.layer.cornerRadius = 15;
    [self.navigationController setNavigationBarHidden:YES];
}
-(IBAction)goScanner:(id)sender{
    QRCodeScannerViewController *QRCodeScannerView = [delegate.Storyboard instantiateViewControllerWithIdentifier:@"QRCodeScannerView"];
    [self.navigationController pushViewController:QRCodeScannerView animated:YES];
}
-(IBAction)goClose:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
